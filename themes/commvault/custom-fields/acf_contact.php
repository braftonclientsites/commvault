<?php
if(function_exists("register_field_group"))
{
	register_field_group(array (
		'id' => 'acf_contact-us',
		'title' => 'Contact Us',
		'fields' => array (
			array (
				'key' => 'field_599deb88b2c68',
				'label' => 'Form Intro',
				'name' => 'form_intro',
				'type' => 'image',
				'save_format' => 'url',
				'preview_size' => 'thumbnail',
				'library' => 'all',
			),
			array (
				'key' => 'field_599deb91b2c69',
				'label' => 'Form',
				'name' => 'form',
				'type' => 'wysiwyg',
				'default_value' => '',
				'toolbar' => 'full',
				'media_upload' => 'yes',
			),
			array (
				'key' => 'field_599deb9bb2c6a',
				'label' => 'Addresses',
				'name' => 'addresses',
				'type' => 'wysiwyg',
				'default_value' => '',
				'toolbar' => 'full',
				'media_upload' => 'yes',
			),
			array (
				'key' => 'field_599deba6b2c6b',
				'label' => 'Offices',
				'name' => 'offices',
				'type' => 'wysiwyg',
				'default_value' => '',
				'toolbar' => 'full',
				'media_upload' => 'yes',
			),
		),
		'location' => array (
			array (
				array (
					'param' => 'page_template',
					'operator' => '==',
					'value' => 'templates/contact.php',
					'order_no' => 0,
					'group_no' => 0,
				),
			),
		),
		'options' => array (
			'position' => 'normal',
			'layout' => 'no_box',
			'hide_on_screen' => array (
			),
		),
		'menu_order' => 0,
	));
}
