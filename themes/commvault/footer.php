<?php
/**
 * The template for displaying the footer
 *
 * Contains the closing of the #content div and all content after.
 *
 * @link https://developer.wordpress.org/themes/basics/template-files/#template-partials
 *
 * @package Bulmapress
 */
?>

</div><!-- #content -->

<div class="clearfix"></div>

<footer id="colophon" class="site-footer hero is-transparent" role="contentinfo">
	<div class="container">
		<div class="hero-body">
			<div class="site-info">
				<div class="menu">
					<?php
					wp_nav_menu(array(
					  'menu' => 'Footer'
					));
					?>
				</div>
				<div class="contact">
					<a class="button" href="<?php echo get_home_url() ?>/contact-us/">Contact Us</a>
					<a class="purple-btn list-opener">Join Our Mailing List</a>
					<div class="mailing-list-form">
						<div class="mailing-list-closer">X</div>
						<?php echo do_shortcode('[gravityform id="3" title="false" description="false"]'); ?>
					</div>
				</div>
				<?php // bulmapress_copyright_link(); ?>
			</div><!-- .site-info -->
		</div>
	</div><!-- .container -->
</footer><!-- #colophon -->
</div><!-- #page -->
<!-- begin Act-On Website Tracking code -->
<script>/*<![CDATA[*/(function(w,a,b,d,s){w[a]=w[a]||{};w[a][b]=w[a][b]||{q:[],track:function(r,e,t){this.q.push({r:r,e:e,t:t||+new Date});}};var e=d.createElement(s);var f=d.getElementsByTagName(s)[0];e.async=1;e.src='//marketing.customvault.com/cdnr/92/acton/bn/tracker/28050';f.parentNode.insertBefore(e,f);})(window,'ActOn','Beacon',document,'script');ActOn.Beacon.track();/*]]>*/</script>
<!-- Google Code for CustomVault Website Tracking Google Conversion Page --> <script type="text/javascript">
/* <![CDATA[ */
var google_conversion_id = 1067379472;
var google_conversion_language = "en";
var google_conversion_format = "3";
var google_conversion_color = "ffffff";
var google_conversion_label = "_kMmCKjVyl4QkNb7_AM"; var google_remarketing_only = false;
/* ]]> */
</script>
<script type="text/javascript"  
src="//www.googleadservices.com/pagead/conversion.js">
</script>
<noscript>
<div style="display:inline;">
<img height="1" width="1" style="border-style:none;" alt="" 
src="//www.googleadservices.com/pagead/conversion/1067379472/?label=_kMmCKjVyl4QkNb7_AM&amp;guid=ON&amp;script=0"/>
</div>
</noscript>
<?php wp_footer(); ?>

</body>
</html>
