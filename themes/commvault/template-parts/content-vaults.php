<?php
/**
 * Template part for displaying page content
 *
 * @link https://codex.wordpress.org/Template_Hierarchy
 *
 * @package Bulmapress
 */
?>

<?php 
if (function_exists('has_post_thumbnail')) {
    if ( has_post_thumbnail() ) {
        $post_image_id = get_post_thumbnail_id($post_to_use->ID);
            if ($post_image_id) {
                $fthumb = wp_get_attachment_image_src( $post_image_id, 'large', false);
                if ($fthumb) (string)$fthumb = $fthumb[0];
            }
    }
} ?>

<article id="post-<?php the_ID(); ?>" <?php post_class('section'); ?>>
	<div class="container content">
        <div class="background" style="background-image: url('<?php echo $fthumb; ?>');">
            <div class="overlay has-text-centered">
                
                <header class="entry-header has-text-centered">
                    <?php the_title( '<h1 class="title is-1 entry-title">', '</h1>' ); ?>
                    <?php echo get_field('main_section_header'); ?>
                </header><!-- .entry-header -->

                <div class="content entry-content">
                    <?php the_content();?>

                    <?php wp_link_pages( array(
                        'before' => '<div class="page-links level">' . esc_html__( 'Pages:', 'bulmapress' ),
                        'after'  => '</div>',
                        ) ); ?>

                    </div><!-- .entry-content -->

                    <?php if ( get_edit_post_link() ) : ?>
                        <footer class="entry-footer">
                            <?php
                            edit_post_link(
                                sprintf(
                                    /* translators: %s: Name of current post */
                                    esc_html__( 'Edit %s', 'bulmapress' ),
                                    the_title( '<span class="screen-reader-text">"', '"</span>', false )
                                    ),
                                '<span class="edit-link">',
                                '</span>'
                                );
                                ?>
                            </footer><!-- .entry-footer -->
                        <?php endif; ?>
                    </div>
                </div>
            </div>
		</article><!-- #post-## -->
        <div class="main">
            <div class="container">
                <div class="first">     
                    <div class="columns has-text-centered">
                            <div class="column is-half-tablet is-half-desktop">
                                <?php echo get_field('col-1'); ?>
                            </div>
                            <div class="column is-half-tablet is-half-desktop">
                                <?php echo get_field('col-2'); ?>
                            </div>
                    </div>
                </div>
            </div>
        </div>

        <div class="second" style="background-image: url('<?php echo get_field('mod_image'); ?>')">
            <div class="overlay">
				<div class="container">    
               		<?php echo get_field('mod_text'); ?>
				</div>
			</div>
		</div>
								
		<div class="third">
			<div class="container">
				<?php echo get_field('mod_blue_text'); ?>
			</div>
		</div>

		<div class="fourth has-text-centered">
			<div class="container">
				<?php echo get_field('sys_text'); ?>
				<div class="columns">
					<div class="column is-half-tablet is-half-desktop">
						<div class="img" style="background-image: url('<?php echo get_field('sys_1_image'); ?>')"></div>
						<?php echo get_field('sys_1_txt'); ?>
					</div>
					<div class="column is-half-tablet is-half-desktop">
						<div class="img" style="background-image: url('<?php echo get_field('sys_2_image'); ?>')"></div>
						<?php echo get_field('sys_2_txt'); ?>
					</div>
					<div class="column is-half-tablet is-half-desktop">
						<div class="img" style="background-image: url('<?php echo get_field('sys_3_image'); ?>')"></div>
						<?php echo get_field('sys_3_txt'); ?>
					</div>
					<div class="column is-half-tablet is-half-desktop">
						<div class="img" style="background-image: url('<?php echo get_field('sys_4_image'); ?>')"></div>
						<?php echo get_field('sys_4_txt'); ?>
					</div>
                    <?php if (get_field('sys_5_image')): ?>
					<div class="column is-half-tablet is-half-desktop">
						<div class="img" style="background-image: url('<?php echo get_field('sys_5_image'); ?>')"></div>
						<?php echo get_field('sys_5_txt'); ?>
					</div>
                    <?php endif; ?>
                    <?php if (get_field('sys_5_image')) : ?>
					<div class="column cta is-half-tablet is-half-desktop">
                    <?php else: ?>
					<div class="column cta">
                    <?php endif; ?>
						<div class="inner">
							<?php echo get_field('sys_cta'); ?>
						</div>
					</div>
				</div>
			</div>
		</div>