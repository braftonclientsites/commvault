<?php
/**
 * Template part for displaying News and Events posts
 *
 * @link https://codex.wordpress.org/Template_Hierarchy
 *
 * @package Bulmapress
 */
?>

<div class="intro">
    <div class="container has-text-centered">
        <h1><?php the_title(); ?></h1>
        <?php the_content(); ?>
    </div>
</div>  

<div class="posts">
    <div class="container">
        <div class="posts">
        <div class="columns">
        <?php 
           $paged = (get_query_var('paged')) ? get_query_var('paged') : 1;
           $wp_query = new WP_Query( array( 
               'paged' => $paged,
               'posts_per_page' => '9',
               'post_type' => array('news', 'events'),
           ) ); 
           if ($wp_query->have_posts() ) : while ( $wp_query->have_posts() ) : $wp_query->the_post(); 
           
            if (function_exists('has_post_thumbnail')) {
                if ( has_post_thumbnail() ) {
                    $post_image_id = get_post_thumbnail_id($post_to_use->ID);
                    if ($post_image_id) {
                        $bthumb = wp_get_attachment_image_src( $post_image_id, 'large', false);
                        if ($bthumb) (string)$bthumb = $bthumb[0];
                    }
                }
            }	
        ?>
        <div class="column is-one-third-desktop is-one-third-tablet">
            <div class="post">
                <div class="meta">
                    <?php 
                    if (get_post_type() == 'events') : ?>
                        <div class="cat">Event</div>
                    <?php endif;
                    if (get_post_type() == 'news') : ?>
                        <div class="cat">News</div>
                    <?php endif; ?>
                    <div class="sep">|</div>
                    <div class="date"><?php echo get_the_date(); ?></div>
                </div>
                <div class="img" style="background-image: url('<?php echo $bthumb; ?>');"></div>
                <div class="content">
                    <h3><a target="_blank" href="<?php echo get_field('attachment'); ?>"><?php the_title(); ?></a></h3>
                    <?php the_excerpt(); ?>
                    <a target="_blank" href="<?php echo get_field('attachment'); ?>">Read more ></a>
                </div>
            </div>
        </div>
        <?php 
        endwhile; ?>
        <div class="section pagination has-text-centered">
            <div class="container">
                <?php the_posts_pagination(); ?>
            </div>
        </div>
        <?php
        endif; wp_reset_query(); ?>
        </div>
        </div>
    </div>
</div>