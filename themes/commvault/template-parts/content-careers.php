<?php
/**
 * Template part for displaying Career posts
 *
 * @link https://codex.wordpress.org/Template_Hierarchy
 *
 * @package Bulmapress
 */
?>

<?php 
if (function_exists('has_post_thumbnail')) {
    if ( has_post_thumbnail() ) {
        $post_image_id = get_post_thumbnail_id($post_to_use->ID);
            if ($post_image_id) {
                $fthumb = wp_get_attachment_image_src( $post_image_id, 'large', false);
                if ($fthumb) (string)$fthumb = $fthumb[0];
            }
    }
} ?>

<article id="post-<?php the_ID(); ?>" <?php post_class('section'); ?>>
<div class="container content">
    <div class="background" style="background-image: url('<?php echo $fthumb; ?>');">
        <div class="overlay has-text-centered">
            <header class="entry-header">
                <?php the_title( '<h1 class="title is-1 entry-title">', '</h1>' ); ?>
            </header><!-- .entry-header -->

            <div class="content entry-content">
                <?php the_content();?>

                <?php 
                // your taxonomy name
                $tax = 'custom_career_cat';
                // get the terms of taxonomy
                $terms = get_terms( $tax, $args = array( 
                'hide_empty' => false, // do not hide empty terms
                ));

                    if ( ! empty( $terms ) && ! is_wp_error( $terms ) ){                

                        // loop through all terms
                        foreach( $terms as $term ) {

                            if( $term->count > 0 )
                                // display link to term archive
                                echo '<a class="button" id="' . $term->slug . '">' . $term->name .'</a>';

                            elseif( $term->count !== 0 )
                                // display name
                                echo '' . $term->name .'';

                            ?>
                            <script>
			                    jQuery(document).ready(function($) {
                                    $('#<?php echo $term->slug; ?>').click(function() {
                                        $('.hidePost').hide();
                                        $('.<?php echo $term->slug; ?>.hidePost').fadeIn();
                                    });
                                });
                            </script>
                            <?php
                        }
                    }
                ?>

                <?php wp_link_pages( array(
                    'before' => '<div class="page-links level">' . esc_html__( 'Pages:', 'bulmapress' ),
                    'after'  => '</div>',
                    ) ); ?>

                </div><!-- .entry-content -->

                <?php if ( get_edit_post_link() ) : ?>
                    <footer class="entry-footer">
                        <?php
                        edit_post_link(
                            sprintf(
                                /* translators: %s: Name of current post */
                                esc_html__( 'Edit %s', 'bulmapress' ),
                                the_title( '<span class="screen-reader-text">"', '"</span>', false )
                                ),
                            '<span class="edit-link">',
                            '</span>'
                            );
                            ?>
                        </footer><!-- .entry-footer -->
                    <?php endif; ?>
                </div>
            </article><!-- #post-## -->
        </div>
    </div>

<div class="posts">
    <div class="container">
        <div class="posts">
        <?php 
           $paged = (get_query_var('paged')) ? get_query_var('paged') : 1;
           $wp_query = new WP_Query( array( 
               'paged' => $paged,
               'posts_per_page' => -1,
               'post_type' => 'careers',
           ) ); 
           if ($wp_query->have_posts() ) : while ( $wp_query->have_posts() ) : $wp_query->the_post(); 
           
            if (function_exists('has_post_thumbnail')) {
                if ( has_post_thumbnail() ) {
                    $post_image_id = get_post_thumbnail_id($post_to_use->ID);
                    if ($post_image_id) {
                        $bthumb = wp_get_attachment_image_src( $post_image_id, 'large', false);
                        if ($bthumb) (string)$bthumb = $bthumb[0];
                    }
                }
            }	
        ?>
        <div class="column hidePost <?php $single_terms = wp_get_post_terms($post->ID, 'custom_career_cat', array("fields" => "all"));
echo $single_terms[0]->slug ; ?>">
            <div class="post">
                <div class="content">
                    <h2><?php the_title(); ?></h2>
                    <?php the_content(); ?>
                    <div class="apply">
                        <a target="_blank" class="button" href="http://www.branchserv.com/info/documents/CombinedDisclosureAuthorizationFormEnglish.pdf">Disclosure Authorization Form ></a>
                        <a target="_blank" class="button" href="http://www.branchserv.com/info/documents/Employment_Application_Federal50StateCompliant-NoSSN.PDF">Apply Now ></a>
                    </div>
                </div>
            </div>
        </div>
        <?php 
        endwhile; else: ?>
        <div class="nothing-found">
            <h2 class="has-text-centered">There are no current openings at CustomVault.</h2>
        </div>
        <?php
        endif; wp_reset_query(); ?>
        </div>
        </div>
    </div>
    

    <div class="subnav-buttons">
            <div class="container has-text-centered">
                <?php echo wp_list_pages( 'sort_column=menu_order&title_li=&child_of=6'); ?>
            </div>
        </div>
</div>