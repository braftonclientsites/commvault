<?php
/**
 * Template part for displaying page content
 *
 * @link https://codex.wordpress.org/Template_Hierarchy
 *
 * @package Bulmapress
 */
?>

<?php 
if (function_exists('has_post_thumbnail')) {
    if ( has_post_thumbnail() ) {
        $post_image_id = get_post_thumbnail_id($post_to_use->ID);
            if ($post_image_id) {
                $fthumb = wp_get_attachment_image_src( $post_image_id, 'large', false);
                if ($fthumb) (string)$fthumb = $fthumb[0];
            }
    }
} ?>

<article id="post-<?php the_ID(); ?>" <?php post_class('section'); ?>>
	<div class="container content">
        <div class="background" style="background-image: url('<?php echo $fthumb; ?>');">
            <div class="overlay has-text-centered">
                <header class="entry-header">
                    <?php the_title( '<h1 class="title is-1 entry-title">', '</h1>' ); ?>
                </header><!-- .entry-header -->

                <div class="content entry-content">
                    <?php the_content();?>

                    <?php wp_link_pages( array(
                        'before' => '<div class="page-links level">' . esc_html__( 'Pages:', 'bulmapress' ),
                        'after'  => '</div>',
                        ) ); ?>

                    </div><!-- .entry-content -->

                    <?php if ( get_edit_post_link() ) : ?>
                        <footer class="entry-footer">
                            <?php
                            edit_post_link(
                                sprintf(
                                    /* translators: %s: Name of current post */
                                    esc_html__( 'Edit %s', 'bulmapress' ),
                                    the_title( '<span class="screen-reader-text">"', '"</span>', false )
                                    ),
                                '<span class="edit-link">',
                                '</span>'
                                );
                                ?>
                            </footer><!-- .entry-footer -->
                        <?php endif; ?>
                    </div>
                </article><!-- #post-## -->
            </div>
        </div>

    <div class="main">
        <div class="image" style="background-image: url('<?php echo get_field('main_image'); ?>');"></div>
        <div class="main-content">
            <div class="inner">
                <?php echo get_field('main_content'); ?>
            </div>
        </div>
    </div>

    <?php if (get_field('features_intro')): ?>
    <div class="feat-intro">
        <div class="container">
            <div class="inner has-text-centered">
                <?php echo get_field('features_intro'); ?>
            </div>
        </div>
    </div>
    <?php endif; ?>

    <div class="container">
        <div class="columns">
            <div class="column is-half-tablet is-one-quarter-desktop">
                <div class="feature">
                    <div class="img" style="background-image: url('<?php echo get_field('feature_1_image'); ?>');"></div>
                    <div class="text has-text-centered">
                        <?php echo get_field('feature_1_description'); ?>
                    </div>
                </div>
            </div>
            <div class="column is-half-tablet is-one-quarter-desktop">
                <div class="feature">
                    <div class="img" style="background-image: url('<?php echo get_field('feature_2_image'); ?>');"></div>
                    <div class="text has-text-centered">
                        <?php echo get_field('feature_2_description'); ?>
                    </div>
                </div>
            </div>
            <div class="column is-half-tablet is-one-quarter-desktop">
                <div class="feature">
                    <div class="img" style="background-image: url('<?php echo get_field('feature_3_image'); ?>');"></div>
                    <div class="text has-text-centered">
                        <?php echo get_field('feature_3_description'); ?>
                    </div>
                </div>
            </div>
            <?php if (get_field('feature_4_description') ) : ?>
            <div class="column is-half-tablet is-one-quarter-desktop">
                <div class="feature">
                    <div class="img" style="background-image: url('<?php echo get_field('feature_4_image'); ?>');"></div>
                    <div class="text has-text-centered">
                        <?php echo get_field('feature_4_description'); ?>
                    </div>
                </div>
            </div>
            <?php endif; ?>
        </div>
    </div>
