/**
 * File customizer.js.
 *
 * Theme Customizer enhancements for a better user experience.
 *
 * Contains handlers to make Theme Customizer preview reload changes asynchronously.
 */

( function( $ ) {
    
    $('.selector .pharma').click(function() {
        $(this).addClass('selected');
        $(this).siblings().removeClass('selected');
        $('#pharma').fadeIn();
        $('#cannabis').hide();
        $('#cc').hide(); 
        $('#other').hide(); 
    });

    $('.selector .cannabis').click(function() {
        $(this).addClass('selected');
        $(this).siblings().removeClass('selected');
        $('#cannabis').fadeIn();
        $('#pharma').hide();
        $('#cc').hide(); 
        $('#other').hide(); 
    });

    $('.selector .cc').click(function() {
        $(this).addClass('selected');
        $(this).siblings().removeClass('selected');
        $('#cc').fadeIn();
        $('#cannabis').hide();
        $('#pharma').hide(); 
        $('#other').hide(); 
    });

    $('.selector .other').click(function() {
        $(this).addClass('selected');
        $(this).siblings().removeClass('selected');
        $('#other').fadeIn();
        $('#cannabis').hide();
        $('#pharma').hide(); 
        $('#cc').hide(); 
    });

} )( jQuery );
    