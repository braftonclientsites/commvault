<?php
/**
 * Template part for displaying home page content
 *
 * @link https://codex.wordpress.org/Template_Hierarchy
 *
 * @package Bulmapress
 */
?>

<?php 
if (function_exists('has_post_thumbnail')) {
    if ( has_post_thumbnail() ) {
        $post_image_id = get_post_thumbnail_id($post_to_use->ID);
            if ($post_image_id) {
                $fthumb = wp_get_attachment_image_src( $post_image_id, 'large', false);
                if ($fthumb) (string)$fthumb = $fthumb[0];
            }
    }
} ?>

<article id="post-<?php the_ID(); ?>" <?php post_class('section'); ?>>
	<div class="container content">
        <div class="background" style="background-image: url('<?php echo $fthumb; ?>');">
            <div class="overlay">
                <header class="entry-header">
                    <?php the_title( '<h1 class="title is-1 entry-title">', '</h1>' ); ?>
                </header><!-- .entry-header -->

                <div class="content entry-content">
                    <?php the_content();?>

                    <?php wp_link_pages( array(
                        'before' => '<div class="page-links level">' . esc_html__( 'Pages:', 'bulmapress' ),
                        'after'  => '</div>',
                        ) ); ?>

                    </div><!-- .entry-content -->

                    <?php if ( get_edit_post_link() ) : ?>
                        <footer class="entry-footer">
                            <?php
                            edit_post_link(
                                sprintf(
                                    /* translators: %s: Name of current post */
                                    esc_html__( 'Edit %s', 'bulmapress' ),
                                    the_title( '<span class="screen-reader-text">"', '"</span>', false )
                                    ),
                                '<span class="edit-link">',
                                '</span>'
                                );
                                ?>
                            </footer><!-- .entry-footer -->
                        <?php endif; ?>
                    </div>
                </article><!-- #post-## -->
            </div>
        </div>

        <div class="features">
            <div class="container">
                <div class="columns">
                    <div class="column is-half-tablet is-one-third-desktop">
                        <div class="feature" style="background-image: url('<?php echo get_field('feature_1_image'); ?>');">
                            <div class="overlay">
                                <div class="front">
                                    <?php echo get_field('feature_1_text'); ?>
                                </div>
                                <div class="back">
                                    <?php echo get_field('feature_1_back'); ?>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="column is-half-tablet is-one-third-desktop">
                        <div class="feature" style="background-image: url('<?php echo get_field('feature_2_image'); ?>');">
                            <div class="overlay">
                                <div class="front">
                                    <?php echo get_field('feature_2_text'); ?>
                                </div>
                                <div class="back">
                                    <?php echo get_field('feature_2_back'); ?>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="column is-half-tablet is-one-third-desktop">
                        <div class="feature" style="background-image: url('<?php echo get_field('feature_3_image'); ?>');">
                            <div class="overlay">
                                <div class="front">
                                    <?php echo get_field('feature_3_text'); ?>
                                </div>
                                <div class="back">
                                    <?php echo get_field('feature_3_back'); ?>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="column is-half-tablet is-one-third-desktop">
                        <div class="feature" style="background-image: url('<?php echo get_field('feature_4_image'); ?>');">
                            <div class="overlay">
                                <div class="front">
                                    <?php echo get_field('feature_4_text'); ?>
                                </div>
                                <div class="back">
                                    <?php echo get_field('feature_4_back'); ?>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="column is-half-tablet is-one-third-desktop">
                        <div class="feature" style="background-image: url('<?php echo get_field('feature_5_image'); ?>');">
                            <div class="overlay">
                                <div class="front">
                                    <?php echo get_field('feature_5_text'); ?>
                                </div>
                                <div class="back">
                                    <?php echo get_field('feature_5_back'); ?>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="column is-half-tablet is-one-third-desktop">
                        <div class="feature" style="background-image: url('<?php echo get_field('feature_6_image'); ?>');">
                            <div class="overlay">
                                <div class="front">
                                    <?php echo get_field('feature_6_text'); ?>
                                </div>
                                <div class="back">
                                    <?php echo get_field('feature_6_back'); ?>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div style="clear: both;"></div>
                </div>
            </div>
        </div>

        <div class="company">
            <div class="container">
                <?php echo get_field('company'); ?>
            </div>
        </div>

        <div class="testimonials">
        <div class="overlay">
            <div class="container">
                <h2>What People Are Saying</h2>
                <div class="selector">
                    <a class="pharma selected">Pharmaceutical</a>
                    <a class="cannabis">Cannabis</a>
                    <a class="cc">Credit Cards</a>
                    <a class="other">Other</a>
                </div>
                <h3>At the end of the day, what the customer says is what matters most to all of us at CustomVault</h3>
                <div id="pharma">
                    <script>
                    jQuery(document).ready(function ($) {
                        $('.bxslider').bxSlider({
                          pager: false,
                          nextSelector: '.next',
                          prevSelector: '.prev',
                          nextText: '>',
                          prevText: '<',
                          auto: true,
                          pause: 8000
                        });
                    });
                    </script>
                    <div class="controls">
                        <div class="next"></div>
                        <div class="prev"></div>
                    </div>
                    <ul class="bxslider">
                    <?php 
                        $testimonials_query = array(
                            'post_type' => 'testimonials', 
                            'orderby' => 'rand',
                            'posts_per_page'=> -1,
                            'tax_query' => array(
                                array (
                                    'taxonomy' => 'custom_cat',
                                    'field' => 'slug',
                                    'terms' => 'pharmaceutical',
                                )
                            )
                        );
                        $test_loop = new WP_QUERY ($testimonials_query);
                        if ($test_loop -> have_posts() ) :
                        while ($test_loop -> have_posts() ) : $test_loop -> the_post(); 	
                    ?>
                    <li>
                        <?php the_content(); ?>
                        <div class="author">
                            <?php echo get_field('title'); ?>
                        </div>
                    </li>
                    <?php endwhile; endif;  wp_reset_postdata(); ?>
                    </ul>
                </div>
                <div id="cannabis" style="display: none;">
                    <script>
                    jQuery(document).ready(function ($) {
                        cannabisSlider = $('.cannabisBxslider').bxSlider({
                          pager: false,
                          nextSelector: '.cannabisNext',
                          prevSelector: '.cannabisPrev',
                          nextText: '>',
                          prevText: '<',
                          auto: true,
                          pause: 8000
                        });
                        // Reload slider to preserve width
                        $('a.cannabis').click(function() {
                            cannabisSlider.reloadSlider();
                        });
                    });
                    </script>
                    <div class="controls">
                        <div class="cannabisNext"></div>
                        <div class="cannabisPrev"></div>
                    </div>
                    <ul class="cannabisBxslider">
                    <?php 
                        $cannabis_testimonials_query = array(
                            'post_type' => 'testimonials', 
                            'orderby' => 'rand',
                            'posts_per_page'=> -1,
                            'tax_query' => array(
                                array (
                                    'taxonomy' => 'custom_cat',
                                    'field' => 'slug',
                                    'terms' => 'cannabis',
                                )
                            )
                        );
                        $cannabis_loop = new WP_QUERY ($cannabis_testimonials_query);
                        if ($cannabis_loop -> have_posts() ) :
                        while ($cannabis_loop -> have_posts() ) : $cannabis_loop -> the_post(); 	
                    ?>
                    <li>
                        <?php the_content(); ?>
                        <div class="author">
                            <?php echo get_field('title'); ?>
                        </div>
                    </li>
                    
                    <?php endwhile; endif;  wp_reset_postdata(); ?>
                    </ul>
                </div>
                <div id="cc" style="display: none;">
                    <script>
                    jQuery(document).ready(function ($) {
                        ccSlider = $('.ccBxslider').bxSlider({
                          pager: false,
                          nextSelector: '.ccNext',
                          prevSelector: '.ccPrev',
                          nextText: '>',
                          prevText: '<',
                          auto: true,
                          pause: 8000
                        });
                        // Reload slider to preserve width
                        $('a.cc').click(function() {
                          ccSlider.reloadSlider();
                        });
                    });
                    </script>
                    <div class="controls">
                        <div class="ccNext"></div>
                        <div class="ccPrev"></div>
                    </div>
                    <ul class="ccBxslider">
                    <?php 
                        $cc_testimonials_query = array(
                            'post_type' => 'testimonials', 
                            'orderby' => 'rand',
                            'posts_per_page'=> -1,
                            'tax_query' => array(
                                array (
                                    'taxonomy' => 'custom_cat',
                                    'field' => 'slug',
                                    'terms' => 'credit-cards',
                                )
                            )
                        );
                        $cc_loop = new WP_QUERY ($cc_testimonials_query);
                        if ($cc_loop -> have_posts() ) :
                        while ($cc_loop -> have_posts() ) : $cc_loop -> the_post(); 	
                    ?>

                    <li>
                        <?php the_content(); ?>
                        <div class="author">
                            <?php echo get_field('title'); ?>
                        </div>
                    </li>
                    
                    <?php endwhile; endif;  wp_reset_postdata(); ?>
                    </ul>
                </div>

                <div id="other" style="display: none;">
                    <script>
                    jQuery(document).ready(function ($) {
                        otherSlider = $('.otherBxslider').bxSlider({
                          pager: false,
                          nextSelector: '.otherNext',
                          prevSelector: '.otherPrev',
                          nextText: '>',
                          prevText: '<',
                          auto: true,
                          pause: 8000
                        });
                        // Reload slider to preserve width
                        $('a.other').click(function() {
                          otherSlider.reloadSlider();
                        });
                    });
                    </script>
                    <div class="controls">
                        <div class="otherNext"></div>
                        <div class="otherPrev"></div>
                    </div>
                    <ul class="otherBxslider">
                    <?php 
                        $other_testimonials_query = array(
                            'post_type' => 'testimonials', 
                            'orderby' => 'rand',
                            'posts_per_page'=> -1,
                            'tax_query' => array(
                                array (
                                    'taxonomy' => 'custom_cat',
                                    'field' => 'slug',
                                    'terms' => 'other',
                                )
                            )
                        );
                        $other_loop = new WP_QUERY ($other_testimonials_query);
                        if ($other_loop -> have_posts() ) :
                        while ($other_loop -> have_posts() ) : $other_loop -> the_post(); 	
                    ?>

                    <li>
                        <?php the_content(); ?>
                        <div class="author">
                            <?php echo get_field('title'); ?>
                        </div>
                    </li>
                    
                    <?php endwhile; endif;  wp_reset_postdata(); ?>
                    </ul>
                </div>
            </div>
        </div>
    </div>

        <div class="blog-posts">

            <div class="container">

                <h2>The Latest from CustomVault</h2>

                <div class="columns">

                <div class="posts column">
                <div class="columns">
                <?php 
                $blog_query = array(
                    'post_type' => array('post', 'news'),
                    'posts_per_page'=> '3',
                );
                $blog_loop = new WP_QUERY ($blog_query);
                if ($blog_loop -> have_posts() ) :
                while ($blog_loop -> have_posts() ) : $blog_loop -> the_post(); 
                if (function_exists('has_post_thumbnail')) {
                    if ( has_post_thumbnail() ) {
                        $post_image_id = get_post_thumbnail_id($post_to_use->ID);
                            if ($post_image_id) {
                                $bthumb = wp_get_attachment_image_src( $post_image_id, 'large', false);
                                if ($bthumb) (string)$bthumb = $bthumb[0];
                            }
                    }
                }	
                ?>

                <div class="column is-one-third-tablet is-one-third-desktop">
                    <div class="post">
                        <div class="meta">
                            <?php 
                            if (get_post_type() == 'events') : ?>
                                <div class="cat">Event</div>
                            <?php endif;
                            if (get_post_type() == 'news') : ?>
                                <div class="cat">News</div>
                            <?php endif; 
                            if (get_post_type() == 'post') : ?>
                                <div class="cat"><?php the_category(', '); ?></div>
                            <?php endif; ?>
                            <div class="sep">|</div>
                            <div class="date"><?php echo get_the_date(); ?></div>
                        </div>
                        <div class="img" style="background-image: url('<?php echo $bthumb; ?>');"></div>
                        <div class="content">
                            <?php if (get_field('attachment')) : ?>
                                <h3><a target="blank" href="<?php echo get_field('attachment'); ?>"><?php the_title(); ?></a></h3>
                            <?php else: ?>
                                <h3><a href="<?php the_permalink(); ?>"><?php the_title(); ?></a></h3>
                            <?php endif; ?>
                            <?php the_excerpt(); ?>
                            <?php if (get_field('attachment')) : ?>
                                <a target="blank" href="<?php echo get_field('attachment'); ?>">Read More ></a>
                            <?php else: ?>
                                <a href="<?php the_permalink(); ?>">Read more ></a>
                            <?php endif; ?>
                        </div>
                    </div>
                </div>

                <?php endwhile; endif; wp_reset_postdata(); ?>
                </div>
                </div>
                </div>

                <div class="social">
                    <a target="_blank" href="https://www.linkedin.com/company/2346623/"><i class="fa fa-linkedin"></i><span class="text">Follow CustomVault on LinkedIn</span></a>
                </div>

            </div>
        </div>


                    
