<?php
/**
 * Template part for displaying blog content
 *
 * @link https://codex.wordpress.org/Template_Hierarchy
 *
 * @package Bulmapress
 */
?>

<div class="intro">
    <div class="container has-text-centered">
        <h1><?php the_title(); ?></h1>
        <?php // the_content(); ?>
    </div>
</div>  

<div class="posts">
    <div class="container">
        <div class="posts">
        <div class="columns">
        <?php 
           $i = 0;
           $paged = (get_query_var('paged')) ? get_query_var('paged') : 1;
           $wp_query = new WP_Query( array( 
               'paged' => $paged,
               'posts_per_page' => -1,
               'post_type' => 'team',
               'order' => ASC,
            ) ); 
           if ($wp_query->have_posts() ) : while ( $wp_query->have_posts() ) : $wp_query->the_post(); 
           
            if (function_exists('has_post_thumbnail')) {
                if ( has_post_thumbnail() ) {
                    $post_image_id = get_post_thumbnail_id($post_to_use->ID);
                    if ($post_image_id) {
                        $bthumb = wp_get_attachment_image_src( $post_image_id, 'large', false);
                        if ($bthumb) (string)$bthumb = $bthumb[0];
                    }
                }
            }	
        ?>
        <?php if ($i == 0) : ?>
            <div class="column first post-<?php echo $i; ?>">
        <?php else: ?>
            <div class="column is-one-half-desktop is-half-tablet post-<?php echo $i; ?>">
        <?php endif; ?>
            <div class="post has-text-centered">
                <div class="flip-container" ontouchstart="this.classList.toggle('hover');">
                    <div class="flipper">
                        <!--<div class="front">
                            <div class="feature" style="background-image: url('<?php echo $bthumb; ?>');"></div>
                        </div>-->
                        <div class="front">
                            <div class="feature">
                                <h3><?php the_title(); ?></h3>
                                <h4><?php echo get_field('title'); ?></h4>
                            </div>
                        </div>
                        <div class="back">
                            <div class="feature">
                                <div class="overlay">
                                    <?php the_content(); ?>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                <!--<h3><?php the_title(); ?></h3>
                <h4><?php echo get_field('title'); ?></h4>-->
            </div>
        </div>
        <?php 
        $i++; endwhile; ?>
        <div class="section pagination has-text-centered">
            <div class="container">
                <?php the_posts_pagination(); ?>
            </div>
        </div>
        <?php
        endif; wp_reset_query(); ?>

        </div>
        </div>
    </div>
    <div class="work">
        <div class="container has-text-centered">
            <?php the_content(); ?>
        </div>
    </div>
    

    <div class="subnav-buttons">
        <div class="container has-text-centered">
            <?php echo wp_list_pages( 'sort_column=menu_order&title_li=&child_of=6'); ?>
        </div>
    </div>
    
</div>