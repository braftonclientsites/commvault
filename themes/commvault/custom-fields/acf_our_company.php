<?php
if(function_exists("register_field_group"))
{
	register_field_group(array (
		'id' => 'acf_our-company',
		'title' => 'Our Company',
		'fields' => array (
			array (
				'key' => 'field_599c64baa2782',
				'label' => 'History',
				'name' => 'history',
				'type' => 'wysiwyg',
				'default_value' => '',
				'toolbar' => 'full',
				'media_upload' => 'yes',
			),
			array (
				'key' => 'field_599c64c5a2783',
				'label' => 'Mission',
				'name' => 'mission',
				'type' => 'wysiwyg',
				'default_value' => '',
				'toolbar' => 'full',
				'media_upload' => 'yes',
			),
			array (
				'key' => 'field_599c64cca2784',
				'label' => 'Responsibility',
				'name' => 'responsibility',
				'type' => 'wysiwyg',
				'default_value' => '',
				'toolbar' => 'full',
				'media_upload' => 'yes',
			),
			array (
				'key' => 'field_599c64d8a2785',
				'label' => 'Work',
				'name' => 'work',
				'type' => 'wysiwyg',
				'default_value' => '',
				'toolbar' => 'full',
				'media_upload' => 'yes',
			),
		),
		'location' => array (
			array (
				array (
					'param' => 'page_template',
					'operator' => '==',
					'value' => 'templates/our-company.php',
					'order_no' => 0,
					'group_no' => 0,
				),
			),
		),
		'options' => array (
			'position' => 'normal',
			'layout' => 'no_box',
			'hide_on_screen' => array (
			),
		),
		'menu_order' => 0,
	));
}
