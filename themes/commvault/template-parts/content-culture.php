<?php
/**
 * Template part for displaying page content
 *
 * @link https://codex.wordpress.org/Template_Hierarchy
 *
 * @package Bulmapress
 */
?>

<?php 
if (function_exists('has_post_thumbnail')) {
    if ( has_post_thumbnail() ) {
        $post_image_id = get_post_thumbnail_id($post_to_use->ID);
            if ($post_image_id) {
                $fthumb = wp_get_attachment_image_src( $post_image_id, 'large', false);
                if ($fthumb) (string)$fthumb = $fthumb[0];
            }
    }
} ?>

<article id="post-<?php the_ID(); ?>" <?php post_class('section'); ?>>
	<div class="container content">
        <div class="background" style="background-image: url('<?php echo $fthumb; ?>');">
            <div class="overlay has-text-centered">

                <div class="content entry-content">
                    <?php the_content();?>

                    <?php wp_link_pages( array(
                        'before' => '<div class="page-links level">' . esc_html__( 'Pages:', 'bulmapress' ),
                        'after'  => '</div>',
                        ) ); ?>

                    </div><!-- .entry-content -->

                    <?php if ( get_edit_post_link() ) : ?>
                        <footer class="entry-footer">
                            <?php
                            edit_post_link(
                                sprintf(
                                    /* translators: %s: Name of current post */
                                    esc_html__( 'Edit %s', 'bulmapress' ),
                                    the_title( '<span class="screen-reader-text">"', '"</span>', false )
                                    ),
                                '<span class="edit-link">',
                                '</span>'
                                );
                                ?>
                            </footer><!-- .entry-footer -->
                        <?php endif; ?>
                    </div>
                </div>
            </div>
		</article><!-- #post-## -->
        <div class="main">
            <div class="container">
                <div class="first">     
                        <header class="entry-header has-text-centered">
                            <?php the_title( '<h1 class="title is-1 entry-title">', '</h1>' ); ?>
                            <?php echo get_field('main_section_header'); ?>
                        </header><!-- .entry-header -->
                        <div class="columns">
                            <div class="column first-col is-half-tablet is-half-desktop">
                                <div class="text">
                                    <?php echo get_field('main_content'); ?>
                                </div>
                                <?php if (get_field('main_image')) : ?>
                                    <div class="img" style="background-image: url('<?php echo get_field('main_image')?>')"></div>
                                <?php endif; ?>
                            </div>
                            <div class="column last-col is-half-tablet is-half-desktop">
                                <?php if (get_field('secondary_image')) : ?>
                                    <div class="img" style="background-image: url('<?php echo get_field('secondary_image')?>')"></div>
                                <?php endif; ?>
                                <div class="text">
                                    <?php echo get_field('secondary_content'); ?>
                                </div>
                            </div>
                        </div>
                </div>
            </div>
        </div>
        
        <?php if (get_field('fourth')) : ?>
            <div class="fourth">
                <div class="container">
                    <div class="text">
                        <?php echo get_field('fourth'); ?>
                    </div>
                    <div class="columns">
                        <div class="column valmain is-half-desktop" style="background-image: url('<?php echo get_field('val_main_image'); ?>');"></div>
                        <div class="column valgrid is-half-desktop">
                            <div class="col first" style="background-image: url('<?php echo get_field('val_1_image'); ?>');">
                                <div class="text">
                                    <div class="front"><?php echo get_field('val_1_text'); ?></div>
                                    <div class="back"><?php echo get_field('val_1_text_back'); ?></div>
                                </div>
                            </div>
                            <div class="col second" style="background-image: url('<?php echo get_field('val_2_image'); ?>');">
                                <div class="text">   
                                    <div class="front"><?php echo get_field('val_2_text'); ?></div>
                                    <div class="back"><?php echo get_field('val_2_text_back'); ?></div>
                                </div>
                            </div>
                            <div class="col" style="background-image: url('<?php echo get_field('val_3_image'); ?>');">
                                <div class="text">
                                    <div class="front"><?php echo get_field('val_3_text'); ?></div>
                                    <div class="back"><?php echo get_field('val_3_text_back'); ?></div>
                                </div>
                            </div>
                            <div class="col" style="background-image: url('<?php echo get_field('val_4_image'); ?>');">
                                <div class="text">
                                    <div class="front"><?php echo get_field('val_4_text'); ?></div>
                                    <div class="back"><?php echo get_field('val_4_text_back'); ?></div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        <?php endif; ?>

        <div class="subnav-buttons">
            <div class="container has-text-centered">
                <?php echo wp_list_pages( 'sort_column=menu_order&title_li=&child_of=6'); ?>
            </div>
        </div>