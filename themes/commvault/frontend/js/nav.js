jQuery(document).ready(function($) {
  $('a.search').click(function() {
    $('.searchForm').fadeToggle();
    $('.searchForm').find('input[type="submit"]').val('Search');
  });
  $('.list-opener').click(function() {
    $('.mailing-list-form').fadeToggle();
    $('.mailing-list-closer').click(function() {
      $('.mailing-list-form').fadeOut();
    });
  });
  var viewport = $(window).width();
  // Mobile scripts
  if (viewport <= 1030) {
    // Toggle 3rd level nav items
    $('li.menu-item-has-children').click(function() {
      $(this).toggleClass('clicked');
    });
    $(document.documentElement).on('click touchend', function(e) {
      if (!$(e.target).closest('.hero').length) {
        if ($('.nav-menu').attr('aria-expanded') === "true") {
          document.getElementsByClassName('nav-menu')[0].setAttribute( 'aria-expanded', 'false' );
          $('#site-navigation').removeClass('toggled');
        }
      }
    });
  } else if (viewport >= 1030) {
    // Wrap submenus in container
    $('li.menu-item-has-children').find('ul.dropdown-menu').wrap('<div class="subContainer" />');
    $('li.menu-item-has-children').each(function() {
      // Create subContainer ctas
      var ctaBtnUrl = $(this).find('a:first-child').attr('href');
      if ($(this).find('a:first-child:contains("Resource")').length) { 
        $(this).find('.subContainer').append('<div class="menuCta"><h2>CustomVault is in the news and in the know.</h2><a class="button" href="' + ctaBtnUrl +'">Learn More ></a></div>');
      } else if ($(this).find('a:first-child:contains("What")').length) {
        $(this).find('.subContainer').append('<div class="menuCta"><h2>CustomVault can meet ALL of your vault and security needs with professionalism and expedience.</h2><a class="button" href="' + ctaBtnUrl +'">Learn More ></a></div>');
      } else if ($(this).find('a:first-child:contains("Industries")').length) {
        $(this).find('.subContainer').append('<div class="menuCta"><h2>No matter what your industry, CustomVault delivers compliant, customized solutions.</h2><a class="button" href="' + ctaBtnUrl +'">Learn More ></a></div>');
      } else {
        $(this).find('.subContainer').append('<div class="menuCta"><h2>CustomVault is the industry expert, employee owned and Responsive by Design.</h2><a class="button" href="' + ctaBtnUrl +'">Learn More ></a></div>');
      }
    });
  }
  $(window).scroll(function() {
    if ($(document).scrollTop() > 50) {
      $('#header').addClass('shrink');
    } else {
      $('#header').removeClass('shrink');
    }
  });
});