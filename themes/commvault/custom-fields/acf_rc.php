<?php
if(function_exists("register_field_group"))
{
	register_field_group(array (
		'id' => 'acf_rc',
		'title' => 'Resource Center',
		'fields' => array (
			array (
				'key' => 'field_59a1e405314f8sec8bottombg',
				'label' => 'Bottom Image',
				'name' => 'bottom_image',
				'type' => 'image',
				'save_format' => 'url',
				'preview_size' => 'thumbnail',
				'library' => 'all',
			),
		),
		'location' => array (
			array (
				array (
					'param' => 'page_template',
					'operator' => '==',
					'value' => 'templates/resource-center.php',
					'order_no' => 0,
					'group_no' => 0,
				),
			),
		),
		'options' => array (
			'position' => 'normal',
			'layout' => 'no_box',
			'hide_on_screen' => array (
			),
		),
		'menu_order' => 0,
	));
}
