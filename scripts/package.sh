#!/usr/bin/env bash

#Change urls for soft_launch
#cd ${GLOBAL_TARGETROOT} && wp search-replace "${GLOBAL_SOFTLAUNCH_URL}" "${GLOBAL_URL}" --recurse-objects --skip-columns=guid  --allow-root
#db dump
cd ${GLOBAL_TARGETROOT} && wp search-replace "${GLOBAL_SOFTLAUNCH_URL}" "${GLOBAL_URL}" --recurse-objects --skip-columns=guid --export=wp-content/${NAME}.sql --allow-root

#wp search-replace foo bar --export=database.sql export after replace???
#zip up files
cd ${GLOBAL_TARGETROOT} && tar -czf ${NAME}.tar.gz ${GLOBAL_TARGETROOT}/wp-content/*

#Change back to continue using dev if needed
#cd ${GLOBAL_TARGETROOT} && wp search-replace "${GLOBAL_URL}" "${GLOBAL_SOFTLAUNCH_URL}" --recurse-objects --skip-columns=guid  --allow-root