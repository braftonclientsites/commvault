<?php
/**
 * Template part for displaying page content
 *
 * @link https://codex.wordpress.org/Template_Hierarchy
 *
 * @package Bulmapress
 */
?>

<?php 
if (function_exists('has_post_thumbnail')) {
    if ( has_post_thumbnail() ) {
        $post_image_id = get_post_thumbnail_id($post_to_use->ID);
            if ($post_image_id) {
                $fthumb = wp_get_attachment_image_src( $post_image_id, 'large', false);
                if ($fthumb) (string)$fthumb = $fthumb[0];
            }
    }
} ?>

<article id="post-<?php the_ID(); ?>" <?php post_class('section'); ?>>
	<div class="container content">
        <div class="background" style="background-image: url('<?php echo $fthumb; ?>');">
            <div class="overlay">

                <div class="content entry-content">
                    <?php the_content();?>

                    <?php wp_link_pages( array(
                        'before' => '<div class="page-links level">' . esc_html__( 'Pages:', 'bulmapress' ),
                        'after'  => '</div>',
                        ) ); ?>

                    </div><!-- .entry-content -->

                    <?php if ( get_edit_post_link() ) : ?>
                        <footer class="entry-footer">
                            <?php
                            edit_post_link(
                                sprintf(
                                    /* translators: %s: Name of current post */
                                    esc_html__( 'Edit %s', 'bulmapress' ),
                                    the_title( '<span class="screen-reader-text">"', '"</span>', false )
                                    ),
                                '<span class="edit-link">',
                                '</span>'
                                );
                                ?>
                            </footer><!-- .entry-footer -->
                        <?php endif; ?>
                    </div>
                </article><!-- #post-## -->
            </div>
        </div>
        
        <div class="form">
            <div class="container">
                <div class="columns">
                    <div class="column img is-one-third-tablet is-one-third-desktop" style="background-image: url('<?php echo get_field('form_intro'); ?>');">                  </div>
                    <div class="column is-two-thirds-tablet is-two-thirds-desktop">
                        <?php echo get_field('form'); ?>
                    </div>
                </div>
            </div>
        </div>

        <div class="addresses">
            <div class="container">
                <div class="columns">
                    <div class="column is-one-quarter-tablet is-one-quarter-desktop">
                        <?php echo get_field('addresses'); ?>
                        <p class="social">
                            <strong>Social Media</strong>
                            <br />
                            <a target="_blank" href="https://www.linkedin.com/company/2346623/">
                                <i class="fa fa-linkedin"></i>
                            </a>
                        </p>
                    </div>
                    <div class="column is-three-quarters-tablet is-three-quarters-desktop">
                        <?php echo get_field('offices'); ?>
                    </div>
                </div>
            </div>
        </div>
