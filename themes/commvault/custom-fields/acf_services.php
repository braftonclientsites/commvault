<?php
if(function_exists("register_field_group"))
{
	register_field_group(array (
		'id' => 'acf_services',
		'title' => 'Services',
		'fields' => array (
			array (
				'key' => 'field_59a1e308314ef',
				'label' => 'Feature 1 Description',
				'name' => 'feature_1_description',
				'type' => 'wysiwyg',
				'default_value' => '',
				'toolbar' => 'full',
				'media_upload' => 'yes',
			),
			array (
				'key' => 'field_59a1e319314f0',
				'label' => 'Feature 1 Image',
				'name' => 'feature_1_image',
				'type' => 'image',
				'save_format' => 'url',
				'preview_size' => 'thumbnail',
				'library' => 'all',
			),
			array (
				'key' => 'field_59a1e325314f1',
				'label' => 'Feature 2 Description',
				'name' => 'feature_2_description',
				'type' => 'wysiwyg',
				'default_value' => '',
				'toolbar' => 'full',
				'media_upload' => 'yes',
			),
			array (
				'key' => 'field_59a1e370314f2',
				'label' => 'Feature 2 Image',
				'name' => 'feature_2_image',
				'type' => 'image',
				'save_format' => 'url',
				'preview_size' => 'thumbnail',
				'library' => 'all',
			),
			array (
				'key' => 'field_59a1e37f314f3',
				'label' => 'Feature 3 Description',
				'name' => 'feature_3_description',
				'type' => 'wysiwyg',
				'default_value' => '',
				'toolbar' => 'full',
				'media_upload' => 'yes',
			),
			array (
				'key' => 'field_59a1e39e314f4',
				'label' => 'Feature 3 Image',
				'name' => 'feature_3_image',
				'type' => 'image',
				'save_format' => 'url',
				'preview_size' => 'thumbnail',
				'library' => 'all',
			),
			array (
				'key' => 'field_59a1e3b0314f5',
				'label' => 'Feature 4 Description',
				'name' => 'feature_4_description',
				'type' => 'wysiwyg',
				'default_value' => '',
				'toolbar' => 'full',
				'media_upload' => 'yes',
			),
			array (
				'key' => 'field_59a1e3dc314f6',
				'label' => 'Feature 4 Image',
				'name' => 'feature_4_image',
				'type' => 'image',
				'save_format' => 'url',
				'preview_size' => 'thumbnail',
				'library' => 'all',
			),
			/*array (
				'key' => 'field_59a1e3ef314f7',
				'label' => 'Feature 5 Description',
				'name' => 'feature_5_description',
				'type' => 'wysiwyg',
				'default_value' => '',
				'toolbar' => 'full',
				'media_upload' => 'yes',
			),
			array (
				'key' => 'field_59a1e405314f8',
				'label' => 'Feature 5 Image',
				'name' => 'feature_5_image',
				'type' => 'image',
				'save_format' => 'url',
				'preview_size' => 'thumbnail',
				'library' => 'all',
			),*/
			array (
				'key' => 'field_59a1e48d314f9',
				'label' => 'Positioned',
				'name' => 'positioned',
				'type' => 'wysiwyg',
				'default_value' => '',
				'toolbar' => 'full',
				'media_upload' => 'yes',
			),
			array (
				'key' => 'field_59a1e499314fa',
				'label' => 'Driven',
				'name' => 'driven',
				'type' => 'wysiwyg',
				'default_value' => '',
				'toolbar' => 'full',
				'media_upload' => 'yes',
			),
			array (
				'key' => 'field_59a1e4b3314fb',
				'label' => 'Testimonial',
				'name' => 'testimonial',
				'type' => 'wysiwyg',
				'default_value' => '',
				'toolbar' => 'full',
				'media_upload' => 'yes',
			),
			array (
				'key' => 'field_59a1e4d2314fc',
				'label' => 'Testimonial Author',
				'name' => 'testimonial_author',
				'type' => 'text',
				'default_value' => '',
				'placeholder' => '',
				'prepend' => '',
				'append' => '',
				'formatting' => 'html',
				'maxlength' => '',
			),
		),
		'location' => array (
			array (
				array (
					'param' => 'page_template',
					'operator' => '==',
					'value' => 'templates/services.php',
					'order_no' => 0,
					'group_no' => 0,
				),
			),
		),
		'options' => array (
			'position' => 'normal',
			'layout' => 'no_box',
			'hide_on_screen' => array (
			),
		),
		'menu_order' => 0,
	));
}
