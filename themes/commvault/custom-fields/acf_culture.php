<?php
if(function_exists("register_field_group"))
{
	register_field_group(array (
		'id' => 'acf_culture',
		'title' => 'Culture',
		'fields' => array (
			array (
				'key' => 'field_599c64baa2782tag',
				'label' => 'Main Section Header',
				'name' => 'main_section_header',
				'type' => 'wysiwyg',
				'default_value' => '',
				'toolbar' => 'full',
				'media_upload' => 'yes',
			),
			array (
				'key' => 'field_599c64baa2782fourth',
				'label' => 'Fourth Section Content',
				'name' => 'fourth',
				'type' => 'wysiwyg',
				'default_value' => '',
				'toolbar' => 'full',
				'media_upload' => 'yes',
			),
			array (
				'key' => 'field_59a1e405314f8valmain',
				'label' => 'Values Main Image',
				'name' => 'val_main_image',
				'type' => 'image',
				'save_format' => 'url',
				'preview_size' => 'thumbnail',
				'library' => 'all',
			),
			array (
				'key' => 'field_59a1e405314f8val1',
				'label' => 'Values Image 1',
				'name' => 'val_1_image',
				'type' => 'image',
				'save_format' => 'url',
				'preview_size' => 'thumbnail',
				'library' => 'all',
			),
			array (
				'key' => 'field_599c64baa27821txt',
				'label' => 'Values Image 1 Text',
				'name' => 'val_1_text',
				'type' => 'wysiwyg',
				'default_value' => '',
				'toolbar' => 'full',
				'media_upload' => 'yes',
			),
			array (
				'key' => 'field_599c64baa27821back',
				'label' => 'Values Image 1 Hover',
				'name' => 'val_1_text_back',
				'type' => 'wysiwyg',
				'default_value' => '',
				'toolbar' => 'full',
				'media_upload' => 'yes',
			),
			array (
				'key' => 'field_59a1e405314f8val2',
				'label' => 'Values Image 2',
				'name' => 'val_2_image',
				'type' => 'image',
				'save_format' => 'url',
				'preview_size' => 'thumbnail',
				'library' => 'all',
			),
			array (
				'key' => 'field_599c64baa27822txt',
				'label' => 'Values Image 2 Text',
				'name' => 'val_2_text',
				'type' => 'wysiwyg',
				'default_value' => '',
				'toolbar' => 'full',
				'media_upload' => 'yes',
			),
			array (
				'key' => 'field_599c64baa27821back2',
				'label' => 'Values Image 2 Hover',
				'name' => 'val_2_text_back',
				'type' => 'wysiwyg',
				'default_value' => '',
				'toolbar' => 'full',
				'media_upload' => 'yes',
			),
			array (
				'key' => 'field_59a1e405314f8val3',
				'label' => 'Values Image 3',
				'name' => 'val_3_image',
				'type' => 'image',
				'save_format' => 'url',
				'preview_size' => 'thumbnail',
				'library' => 'all',
			),
			array (
				'key' => 'field_599c64baa27823txt',
				'label' => 'Values 3 Text',
				'name' => 'val_3_text',
				'type' => 'wysiwyg',
				'default_value' => '',
				'toolbar' => 'full',
				'media_upload' => 'yes',
			),
			array (
				'key' => 'field_599c64baa27821back3',
				'label' => 'Values Image 3 Hover',
				'name' => 'val_3_text_back',
				'type' => 'wysiwyg',
				'default_value' => '',
				'toolbar' => 'full',
				'media_upload' => 'yes',
			),
			array (
				'key' => 'field_59a1e405314f8val4',
				'label' => 'Values Image 4',
				'name' => 'val_4_image',
				'type' => 'image',
				'save_format' => 'url',
				'preview_size' => 'thumbnail',
				'library' => 'all',
			),
			array (
				'key' => 'field_599c64baa27824txt',
				'label' => 'Values Image 4 Text',
				'name' => 'val_4_text',
				'type' => 'wysiwyg',
				'default_value' => '',
				'toolbar' => 'full',
				'media_upload' => 'yes',
			),
			array (
				'key' => 'field_599c64baa27821back4',
				'label' => 'Values Image 4 Hover',
				'name' => 'val_4_text_back',
				'type' => 'wysiwyg',
				'default_value' => '',
				'toolbar' => 'full',
				'media_upload' => 'yes',
			),
		),
		'location' => array (
			array (
				array (
					'param' => 'page_template',
					'operator' => '==',
					'value' => 'templates/culture.php',
					'order_no' => 0,
					'group_no' => 0,
				),
			),
		),
		'options' => array (
			'position' => 'normal',
			'layout' => 'no_box',
			'hide_on_screen' => array (
			),
		),
		'menu_order' => 1,
	));
}