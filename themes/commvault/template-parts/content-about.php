<?php
/**
 * Template part for displaying page content
 *
 * @link https://codex.wordpress.org/Template_Hierarchy
 *
 * @package Bulmapress
 */
?>

<?php 
if (function_exists('has_post_thumbnail')) {
    if ( has_post_thumbnail() ) {
        $post_image_id = get_post_thumbnail_id($post_to_use->ID);
            if ($post_image_id) {
                $fthumb = wp_get_attachment_image_src( $post_image_id, 'large', false);
                if ($fthumb) (string)$fthumb = $fthumb[0];
            }
    }
} ?>

<article id="post-<?php the_ID(); ?>" <?php post_class('section'); ?>>
	<div class="container content">
        <div class="background" style="background-image: url('<?php echo $fthumb; ?>');">
            <div class="overlay">
                <header class="entry-header">
                    <?php the_title( '<h1 class="title is-1 entry-title">', '</h1>' ); ?>
                </header><!-- .entry-header -->

                <div class="content entry-content">
                    <?php the_content();?>

                    <?php wp_link_pages( array(
                        'before' => '<div class="page-links level">' . esc_html__( 'Pages:', 'bulmapress' ),
                        'after'  => '</div>',
                        ) ); ?>

                    </div><!-- .entry-content -->

                    <?php if ( get_edit_post_link() ) : ?>
                        <footer class="entry-footer">
                            <?php
                            edit_post_link(
                                sprintf(
                                    /* translators: %s: Name of current post */
                                    esc_html__( 'Edit %s', 'bulmapress' ),
                                    the_title( '<span class="screen-reader-text">"', '"</span>', false )
                                    ),
                                '<span class="edit-link">',
                                '</span>'
                                );
                                ?>
                            </footer><!-- .entry-footer -->
                        <?php endif; ?>
                    </div>
                </article><!-- #post-## -->
            </div>
        </div>

        <div class="history">
            <div class="container">
                <?php echo get_field('history'); ?>
            </div>
        </div>
        
        <div class="mission">
            <div class="container">
                <?php echo get_field('mission'); ?>
            </div>
        </div>

        <div class="responsibility">
            <div class="overlay">
                <div class="container">
                    <?php echo get_field('responsibility'); ?>
                </div>
            </div>
        </div>

        <div class="work">
            <div class="container">
                <?php echo get_field('work'); ?>
            </div>
        </div>
