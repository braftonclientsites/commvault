<?php
/**
 * The template for displaying archive pages
 *
 * @link https://codex.wordpress.org/Template_Hierarchy
 *
 * @package Bulmapress
 */
?>

<?php get_header(); ?>

<div id="primary" class="content-area">
	<main id="main" class="site-main" role="main">
		<div class="page">
		<?php if ( have_posts() ) : ?>
			<div class="intro">
				<div class="container has-text-centered">
					<?php the_archive_title( '<h1>', '</h1>' ); ?>
				</div>
			</div>
			<div class="posts">
    			<div class="container">
        			<div class="posts">
        				<div class="columns">
							<?php while ( have_posts() ) : the_post(); 
							if (function_exists('has_post_thumbnail')) {
								if ( has_post_thumbnail() ) {
									$post_image_id = get_post_thumbnail_id($post_to_use->ID);
									if ($post_image_id) {
										$bthumb = wp_get_attachment_image_src( $post_image_id, 'large', false);
										if ($bthumb) (string)$bthumb = $bthumb[0];
									}
								}
							}	
							?>
							<div class="column is-one-third-desktop is-one-third-tablet">
								<div class="post">
									<div class="meta">
										<div class="cat"><?php the_category(', '); ?></div>
										<div class="sep">|</div>
										<div class="date"><?php echo get_the_date(); ?></div>
									</div>
									<div class="img" style="background-image: url('<?php echo $bthumb; ?>');"></div>
									<div class="content">
										<?php if (get_field('attachment')) : ?>
											<h3><a target="blank" href="<?php echo get_field('attachment'); ?>"><?php the_title(); ?></a></h3>
										<?php else: ?>
											<h3><a href="<?php the_permalink(); ?>"><?php the_title(); ?></a></h3>
										<?php endif; ?>
										<?php the_excerpt(); ?>
										<?php if (get_field('attachment')) : ?>
											<a target="blank" href="<?php echo get_field('attachment'); ?>">Read More ></a>
										<?php else: ?>
											<a href="<?php the_permalink(); ?>">Read more ></a>
										<?php endif; ?>
									</div>
								</div>
							</div>
						<?php endwhile; ?>
			
						<div class="section pagination has-text-centered">
							<div class="container">
								<?php the_posts_navigation(); ?>
							</div>
						</div>
						<?php else : ?>
						<?php get_template_part( 'template-parts/content', 'none' ); ?>
						<?php endif; ?>
					</div>
				</div>
			</div>
		</div>
		</div>
	</main><!-- #main -->
</div><!-- #primary -->

<?php get_footer();?>