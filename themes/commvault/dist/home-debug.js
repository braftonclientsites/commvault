/**
 * File home.js.
 */

( function( $ ) {

    var viewport = $(window).width();

    if (viewport < 1030) {
        $('.features .column .feature').click(function() {
            $(this).find('.overlay').toggleClass('scrollToTop');
        });
        $('.who .columns .column .feature').click(function() {
            $(this).find('.overlay').toggleClass('scrollToTop');
        });
    }

} )( jQuery );
    