<?php
/**
 * Template part for displaying blog content
 *
 * @link https://codex.wordpress.org/Template_Hierarchy
 *
 * @package Bulmapress
 */
?>

<div class="intro">
    <div class="container has-text-centered">
        <h1><?php the_title(); ?></h1>
        <?php the_content(); ?>
    </div>
</div>  

<div class="posts">
    <div class="container">
        <div class="posts">
        <div class="columns">
        <?php 
           $paged = (get_query_var('paged')) ? get_query_var('paged') : 1;
           $wp_query = new WP_Query( array( 
               'paged' => $paged,
               'posts_per_page' => '9',
               'post_type' => array('news', 'events', 'post'),
           ) ); 
           if ($wp_query->have_posts() ) : while ( $wp_query->have_posts() ) : $wp_query->the_post(); 
           
            if (function_exists('has_post_thumbnail')) {
                if ( has_post_thumbnail() ) {
                    $post_image_id = get_post_thumbnail_id($post_to_use->ID);
                    if ($post_image_id) {
                        $bthumb = wp_get_attachment_image_src( $post_image_id, 'large', false);
                        if ($bthumb) (string)$bthumb = $bthumb[0];
                    }
                }
            }	
        ?>
        <div class="column is-one-third-desktop is-one-third-tablet">
            <div class="post">
                <div class="meta">
                    <?php 
                    if (get_post_type() == 'events') : ?>
                        <div class="cat">Event</div>
                    <?php endif;
                    if (get_post_type() == 'news') : ?>
                        <div class="cat">News</div>
                    <?php endif; ?>
                    <div class="cat"><?php the_category(', '); ?></div>
                    <?php if (!get_field('exclude_date')): ?>
                    <div class="sep">|</div>
                    <div class="date"><?php echo get_the_date(); ?></div>
                    <?php endif; ?>
                </div>
                <div class="img" <?php if (get_field('nat_img')): ?>style="background-image: url('<?php echo $bthumb; ?>'); background-size: contain; background-repeat: no-repeat;"<?php else: ?>style="background-image: url('<?php echo $bthumb; ?>');"<?php endif; ?>></div>
                <div class="content">
                    <?php if (get_field('attachment')) : ?>
                        <h3><a target="blank" href="<?php echo get_field('attachment'); ?>"><?php the_title(); ?></a></h3>
                    <?php else: ?>
                        <h3><a href="<?php the_permalink(); ?>"><?php the_title(); ?></a></h3>
                    <?php endif; ?>
                    <?php the_excerpt(); ?>
                    <?php if (get_field('attachment')) : ?>
                        <a target="blank" href="<?php echo get_field('attachment'); ?>">Read More ></a>
                    <?php else: ?>
                        <a href="<?php the_permalink(); ?>">Read more ></a>
                    <?php endif; ?>
                </div>
            </div>
        </div>
        <?php 
        endwhile; ?>
        <div class="section pagination has-text-centered">
            <div class="container">
                <?php the_posts_pagination(); ?>
            </div>
        </div>
        <?php
        endif; wp_reset_query(); ?>
        </div>
        </div>
    </div>
</div>