<?php
/**
 * The template for displaying all single posts
 *
 * @link https://developer.wordpress.org/themes/basics/template-hierarchy/#single-post
 *
 * @package Bulmapress
 */

get_header(); ?>

<div id="primary" class="content-area">
	<main id="main" class="site-main wrapper" role="main">
		<?php while ( have_posts() ) : the_post(); ?>
			<?php get_template_part( 'template-parts/content', 'post' ); ?>
			<?php if (is_singular('post')): ?>
			<div class="pagination">
				<?php the_post_navigation();?>
			</div>
			<?php endif; ?>
		<?php endwhile; ?>
	</main><!-- #main -->
	<?php get_sidebar(); ?>
</div><!-- #primary -->
<?php get_footer(); ?>
