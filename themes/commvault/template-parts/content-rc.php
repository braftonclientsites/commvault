<?php
/**
 * Template part for displaying blog content
 *
 * @link https://codex.wordpress.org/Template_Hierarchy
 *
 * @package Bulmapress
 */
?>

<?php 
if (function_exists('has_post_thumbnail')) {
    if ( has_post_thumbnail() ) {
        $post_image_id = get_post_thumbnail_id($post_to_use->ID);
            if ($post_image_id) {
                $fthumb = wp_get_attachment_image_src( $post_image_id, 'large', false);
                if ($fthumb) (string)$fthumb = $fthumb[0];
            }
    }
} ?>

<article id="post-<?php the_ID(); ?>" <?php post_class(array('section', 'expertise')); ?>>
<div class="container content">
    <div class="background" style="background-image: url('<?php echo $fthumb; ?>');">
        <div class="overlay has-text-centered">
            <header class="entry-header">
                <?php the_title( '<h1 class="title is-1 entry-title">', '</h1>' ); ?>
            </header><!-- .entry-header -->

            <div class="content entry-content">
                <?php the_content();?>

                <?php wp_link_pages( array(
                    'before' => '<div class="page-links level">' . esc_html__( 'Pages:', 'bulmapress' ),
                    'after'  => '</div>',
                    ) ); ?>

                </div><!-- .entry-content -->

                <?php if ( get_edit_post_link() ) : ?>
                    <footer class="entry-footer">
                        <?php
                        edit_post_link(
                            sprintf(
                                /* translators: %s: Name of current post */
                                esc_html__( 'Edit %s', 'bulmapress' ),
                                the_title( '<span class="screen-reader-text">"', '"</span>', false )
                                ),
                            '<span class="edit-link">',
                            '</span>'
                            );
                            ?>
                        </footer><!-- .entry-footer -->
                    <?php endif; ?>
                </div>
            </div>
        </div>
    </article><!-- #post-## -->

    <div class="posts event-news-posts">
    <div class="container">
        <div class="posts">
        <div class="columns">
        <?php 
           $wp_query = new WP_Query( array( 
               'paged' => $paged,
               'posts_per_page' => '3',
               'post_type' => 'events',
           ) ); 
           if ($wp_query->have_posts() ) : while ( $wp_query->have_posts() ) : $wp_query->the_post(); 
           
            if (function_exists('has_post_thumbnail')) {
                if ( has_post_thumbnail() ) {
                    $post_image_id = get_post_thumbnail_id($post_to_use->ID);
                    if ($post_image_id) {
                        $nthumb = wp_get_attachment_image_src( $post_image_id, 'large', false);
                        if ($nthumb) (string)$nthumb = $nthumb[0];
                    }
                }
            }	
        ?>
        <div class="column is-one-third-desktop is-one-third-tablet">
            <div class="post">
                <div class="meta">
                    <?php 
                    if (get_post_type() == 'events') : ?>
                        <div class="cat">Event</div>
                    <?php endif;
                    if (get_post_type() == 'news') : ?>
                        <div class="cat">News</div>
                    <?php endif; 
                    if (get_post_type() == 'post') : ?>
                        <div class="cat"><?php the_category(', '); ?></div>
                    <?php endif; ?>
                    <div class="sep">|</div>
                    <div class="date"><?php echo get_the_date(); ?></div>
                </div>
                <div class="img" style="background-image: url('<?php echo $nthumb; ?>');"></div>
                <div class="content">
                    <?php if (get_field('attachment')): ?>
                        <h3><a target="_blank" href="<?php echo get_field('attachment'); ?>"><?php the_title(); ?></a></h3>
                        <?php the_excerpt(); ?>
                        <a target="_blank" href="<?php echo get_field('attachment'); ?>">Read more ></a>
                    <?php else: ?>
                        <h3><a href="<?php the_permalink(); ?>"><?php the_title(); ?></a></h3>
                        <?php the_excerpt(); ?>
                        <a href="<?php the_permalink(); ?>">Read more ></a>
                    <?php endif; ?>
                </div>
            </div>
        </div>
        <?php 
        endwhile; endif; wp_reset_query(); ?>
            </div>
        </div>
    </div>
</div>

<div class="posts solutions">
    <div class="container">
        <div class="posts">
        <div class="columns">
        <?php 
           $wp_query = new WP_Query( array( 
               'paged' => $paged,
               'posts_per_page' => '2',
               'post_type' => array('fact-sheets', 'products')
           ) ); 
           if ($wp_query->have_posts() ) : while ( $wp_query->have_posts() ) : $wp_query->the_post(); 
           
            if (function_exists('has_post_thumbnail')) {
                if ( has_post_thumbnail() ) {
                    $post_image_id = get_post_thumbnail_id($post_to_use->ID);
                    if ($post_image_id) {
                        $nthumb = wp_get_attachment_image_src( $post_image_id, 'large', false);
                        if ($nthumb) (string)$nthumb = $nthumb[0];
                    }
                }
            }	
        ?>
        <div class="column is-one-third-desktop is-one-third-tablet">
            <div class="post">
                <div class="meta">
                    <?php 
                    if (get_post_type() == 'fact-sheets') : ?>
                        <div class="cat">Fact Sheet</div>
                    <?php endif;
                    if (get_post_type() == 'products') : ?>
                        <div class="cat">Product</div>
                    <?php endif; 
                    if (get_post_type() == 'post') : ?>
                        <div class="cat"><?php the_category(', '); ?></div>
                    <?php endif; ?>
                </div>
                <div class="img" style="background-image: url('<?php echo $nthumb; ?>');"></div>
                <div class="content">
                    <h3><a target="_blank" href="<?php echo get_field('attachment'); ?>"><?php the_title(); ?></a></h3>
                    <?php the_excerpt(); ?>
                    <a target="_blank" href="<?php echo get_field('attachment'); ?>">Read more ></a>
                </div>
            </div>
        </div>
        <?php 
        endwhile; endif; wp_reset_query(); ?>
                <div class="column more is-one-third-desktop is-one-third-tablet">
                    <div class="inner">
                        <h2>Solutions Center</h2>
                        <a class="white-btn" href="<?php echo get_home_url(); ?>/resource-center/solution-center/">Read More ></a>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>

<div class="blog-posts">
    <div class="container">
        <div class="posts">
            <div class="columns">
            <?php 
            $wp_query = new WP_Query( array( 
               'paged' => $paged,
               'posts_per_page' => '2',
               'post_type' => array('post', 'news')
            ) ); 
            if ($wp_query->have_posts() ) : while ( $wp_query->have_posts() ) : $wp_query->the_post(); 
           
            if (function_exists('has_post_thumbnail')) {
                if ( has_post_thumbnail() ) {
                    $post_image_id = get_post_thumbnail_id($post_to_use->ID);
                    if ($post_image_id) {
                        $bthumb = wp_get_attachment_image_src( $post_image_id, 'large', false);
                        if ($bthumb) (string)$bthumb = $bthumb[0];
                    }
                }
            }	
            ?>
                <div class="column is-one-third-desktop is-one-third-tablet">
                    <div class="post">
                        <div class="meta">
                            <?php 
                            if (get_post_type() == 'events') : ?>
                                <div class="cat">Event</div>
                            <?php endif;
                            if (get_post_type() == 'news') : ?>
                                <div class="cat">News</div>
                            <?php endif; 
                            if (get_post_type() == 'post') : ?>
                                <div class="cat"><?php the_category(', '); ?></div>
                            <?php endif; ?>
                            <div class="sep">|</div>
                            <div class="date"><?php echo get_the_date(); ?></div>
                        </div>
                        <div class="img" style="background-image: url('<?php echo $bthumb; ?>');"></div>
                        <div class="content">
                            <?php if (get_field('attachment')) : ?>
                                <h3><a target="blank" href="<?php echo get_field('attachment'); ?>"><?php the_title(); ?></a></h3>
                            <?php else: ?>
                                <h3><a href="<?php the_permalink(); ?>"><?php the_title(); ?></a></h3>
                            <?php endif; ?>
                            <?php the_excerpt(); ?>
                            <?php if (get_field('attachment')) : ?>
                                <a target="blank" href="<?php echo get_field('attachment'); ?>">Read More ></a>
                            <?php else: ?>
                                <a href="<?php the_permalink(); ?>">Read more ></a>
                            <?php endif; ?>
                        </div>
                    </div>
                </div>
            <?php endwhile; endif; wp_reset_query(); ?>
                <div class="column more is-one-third-desktop is-one-third-tablet">
                    <div class="inner">
                        <h2>Expert Insights</h2>
                        <a class="white-btn" href="<?php echo get_home_url(); ?>/expert-insights/">Read More ></a>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>

<div class="bottom-graphic" style="background-image: url('<?php echo get_field('bottom_image'); ?>');"><div class="overlay"></div></div>