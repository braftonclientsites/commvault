var gulp        = require('gulp');
var sass        = require('gulp-sass');
var minify      = require('gulp-minify');
//var cleanCSS    = require('gulp-clean-css');

// Compile sass into CSS & auto-inject into browsers
gulp.task('sass', function() {
    return gulp.src("frontend/bulmapress/scss/*.scss")
        .pipe(sass())
        .pipe(gulp.dest("frontend/bulmapress/css"))
});

// Watchers
gulp.task('watch', function() {
  gulp.watch(['frontend/bulmapress/scss/*.scss', 'frontend/bulmapress/scss/**/*.scss', '*.php', 'frontend/js/*.js'], ['sass']);
});
 
gulp.task('compress', function() {
  gulp.src('frontend/js/*.js')
    .pipe(minify({
        ext:{
            src:'-debug.js',
            min:'.js'
        },
        exclude: ['tasks'],
        ignoreFiles: ['.combo.js', '-min.js']
    }))
    .pipe(gulp.dest('dist'))
});

gulp.task('default', ['watch', 'compress']);