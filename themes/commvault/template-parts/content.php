<?php
/**
 * Template part for displaying content
 *
 * @link https://codex.wordpress.org/Template_Hierarchy
 *
 * @package Bulmapress
 */
?>

<article id="post-<?php the_ID(); ?>" <?php post_class('section'); ?>>
	<div class="container">
		<header class="content has-text-centered">
			<?php if ( is_single() ) : ?>
				<?php bulmapress_the_title('is-1', FALSE); ?>
			<?php elseif ( 'page' === get_post_type() ) : ?>
				<?php bulmapress_the_title('is-2', FALSE); ?>
			<?php else : ?>
				<?php bulmapress_the_title('is-2'); ?>
			<?php endif; ?>
			<?php if ( 'post' === get_post_type() ) : ?>
				<div class="subtitle is-6">
					<?php bulmapress_posted_on(); ?>
				</div><!-- .entry-meta -->
			<?php endif; ?>
		</header><!-- .entry-header -->

		<div class="content entry-content has-text-centered">
				<?php the_content( sprintf(
				/* translators: %s: Name of current post. */
				wp_kses(
					__( 'Continue reading %s <span class="meta-nav">&rarr;</span>', 'bulmapress' ),
					array(
						'span' => array(
							'class' => array()
							)
						)
					),
				the_title( '<span class="screen-reader-text">"', '"</span>', false )
				)
				);

				wp_link_pages( array(
					'before' => '<div class="page-links">' . esc_html__( 'Pages:', 'bulmapress' ),
					'after'  => '</div>',
					) );
					?>
		</div><!-- .entry-content -->

			<footer class="content entry-footer">
				<?php bulmapress_entry_footer(); ?>
			</footer><!-- .entry-footer -->
		</div>
	</article><!-- #post-## -->

	<div class="main">
		<div class="container">
			<div id="first"></div>
			<div class="first">
				<?php if (get_field('main_image')) : ?>
					<div class="img" style="<?php if (get_field('shrink')): ?>background-size: 100%;<?php endif; ?> background-image: url('<?php echo get_field('main_image')?>');"></div>
				<?php endif; ?>
				<div class="text">
					<?php echo get_field('main_content'); ?>
				</div>
			</div>
			<?php if (get_field('secondary_content') ) : ?>
			<div id="second"></div>
			<div class="second">
				<?php if (get_field('secondary_image')) : ?>
					<div class="img" style="<?php if (get_field('shrink2')): ?>background-size: 100%;<?php endif; ?> background-image: url('<?php echo get_field('secondary_image')?>');"></div>
				<?php endif; ?>
				<div class="text">
					<?php echo get_field('secondary_content'); ?>
				</div>
			</div>
			<?php endif; ?>
			<?php if (get_field('tertiary_content') ) : ?>
			<div id="third"></div>
			<div class="third">
				<?php if (get_field('tertiary_image')) : ?>
					<div class="img" style="<?php if (get_field('shrink3')): ?>background-size: 100%;<?php endif; ?> background-image: url('<?php echo get_field('tertiary_image')?>');"></div>
				<?php endif; ?>
				<div class="text">
					<?php echo get_field('tertiary_content'); ?>
					<div class="solution-buttons">
						<ul class="has-text-centered">
							<?php
							// Load Modular Vaults links on child pages
							if ($post -> post_parent == '18'): ?>
								<?php echo wp_list_pages( 'sort_column=menu_order&title_li=&child_of=18'); ?>
							<?php
							// Load What We Do links on child pages
							elseif ($post -> post_parent == '16' || $post -> post_parent == '46'): ?>
								<?php echo wp_list_pages( 'sort_column=menu_order&title_li=&child_of=16'); ?>
							<?php 
							// Do nothing on other pages
							else: 
								echo '';
							endif; ?>
						</ul>
					</div>
				</div>
			</div>
			<?php endif; ?>
			<?php 
			// Create option for fourth content field for Solution pages (medical marijuana)
			if (get_field('fourth_content') ) : ?>
			<div id="fourth"></div>
			<div class="fourth">
				<?php if (get_field('fourth_image')) : ?>
					<div class="img" style="<?php if (get_field('shrink4')): ?>background-size: 100%;<?php endif; ?> background-image: url('<?php echo get_field('fourth_image')?>');"></div>
				<?php endif; ?>
				<div class="text">
					<?php echo get_field('fourth_content'); ?>
				</div>
			</div>
			<?php endif; ?>
			<?php if (get_field('cta_content') ) : ?>
			<div id="cta" class="third">
				<div class="text">
					<?php echo get_field('cta_content'); ?>
				</div>
			</div>
			<?php endif; ?>
			<?php
			// Load nav buttons below cta on Solution Center subpages
			if ($post -> post_parent == '69'): ?>
			<div class="third">
				<div class="text">
					<div class="solution-buttons">
						<ul class="has-text-centered">
							<?php if ($post -> post_parent == '69'): ?>
								<?php echo wp_list_pages( 'sort_column=menu_order&title_li=&child_of=69'); ?>
							<?php else: 
								echo '';
							endif; ?>
						</ul>
					</div>
				</div>
			</div>
			<?php endif; ?>
			<?php
			// Load the Industries Served links on child pages
			$current = $post->ID;
			$parent = $post->post_parent;
			$id = 46;

			if ($parent == $id):
			?>

			<div class="nav-buttons has-text-centered">
				<?php echo do_shortcode('[wpb_childpages]'); ?>
			</div>

			<?php endif; ?>
			</div>
		</div>
	</div>
