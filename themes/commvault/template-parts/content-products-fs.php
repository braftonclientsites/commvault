<?php
/**
 * Template part for displaying News and Events posts
 *
 * @link https://codex.wordpress.org/Template_Hierarchy
 *
 * @package Bulmapress
 */
?>

<div class="intro">
    <div class="container has-text-centered">
        <h1><?php the_title(); ?></h1>
        <?php the_content(); ?>
        <div class="solution-buttons">
            <ul>
                <?php echo wp_list_pages( 'sort_column=menu_order&title_li=&child_of=69'); ?>
            </ul>
        </div>
    </div>
</div>  

<div class="posts">
    <div class="container">
        <div class="posts">
            <div class="columns">
            <?php 
            $terms = get_terms([
                'taxonomy' => 'custom_product_cat',
                'orderby' => 'count',
                'order' => 'DESC'
            ]); 
            $i = 0;
            foreach ($terms as $term) :
                $term_query[$i] = new WP_Query(array(
                    'posts_per_page' => -1,
                    'tax_query' => array(
                        array (
                            'taxonomy' => 'custom_product_cat',
                            'field' => 'slug',
                            'terms' => $term->slug,
                        ),
                    ),
                ) );
                if ($term_query[$i]->have_posts() ) : ?>
                
                <div class="section-header">
                    <h3><?php echo $term->name; ?></h3>
                </div>
                
                <?php 
                while ( $term_query[$i]->have_posts() ) : $term_query[$i]->the_post(); 
                if (function_exists('has_post_thumbnail')) {
                    if ( has_post_thumbnail() ) {
                        $post_image_id = get_post_thumbnail_id($post_to_use->ID);
                        if ($post_image_id) {
                            $othumb = wp_get_attachment_image_src( $post_image_id, 'large', false);
                            if ($othumb) (string)$othumb = $othumb[0];
                        }
                    }
                }	
            ?>
                <div class="column is-one-third-desktop is-one-third-tablet">
                    <div class="post">
                        <div class="meta">
                            <div class="cat"><?php echo $term->name ?></div>
                        </div>
                        <div class="img" style="background-image: url('<?php echo $othumb; ?>');"></div>
                        <div class="content">
                            <?php if (get_field('attachment')) : ?>
                                <h3><a target="_blank" href="<?php echo get_field('attachment'); ?>"><?php the_title(); ?></a></h3>
                            <?php else: ?>
                                <h3><a href="<?php the_permalink(); ?>"><?php the_title(); ?></a></h3>
                            <?php endif; ?>
                            <?php the_excerpt(); ?>
                            <a target="_blank" href="<?php echo get_field('attachment'); ?>">Read more ></a>
                        </div>
                    </div>
                </div>
                <?php 
                endwhile; $i++; ?>
                <?php
                endif; 
            endforeach;
        wp_reset_query(); ?>
        </div>
        </div>
    </div>
</div>