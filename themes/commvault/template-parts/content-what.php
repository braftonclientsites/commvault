<?php
/**
 * Template part for displaying page content
 *
 * @link https://codex.wordpress.org/Template_Hierarchy
 *
 * @package Bulmapress
 */
?>

<?php 
if (function_exists('has_post_thumbnail')) {
    if ( has_post_thumbnail() ) {
        $post_image_id = get_post_thumbnail_id($post_to_use->ID);
            if ($post_image_id) {
                $fthumb = wp_get_attachment_image_src( $post_image_id, 'large', false);
                if ($fthumb) (string)$fthumb = $fthumb[0];
            }
    }
} ?>

<article id="post-<?php the_ID(); ?>" <?php post_class('section'); ?>>
	<div class="container content">
        <div class="background" style="background-image: url('<?php echo $fthumb; ?>');">
            <div class="overlay">
                <header class="entry-header">
                    <?php the_title( '<h1 class="title is-1 entry-title">', '</h1>' ); ?>
                </header><!-- .entry-header -->

                <div class="content entry-content">
                    <?php the_content();?>

                    <?php wp_link_pages( array(
                        'before' => '<div class="page-links level">' . esc_html__( 'Pages:', 'bulmapress' ),
                        'after'  => '</div>',
                        ) ); ?>

                    </div><!-- .entry-content -->

                    <?php if ( get_edit_post_link() ) : ?>
                        <footer class="entry-footer">
                            <?php
                            edit_post_link(
                                sprintf(
                                    /* translators: %s: Name of current post */
                                    esc_html__( 'Edit %s', 'bulmapress' ),
                                    the_title( '<span class="screen-reader-text">"', '"</span>', false )
                                    ),
                                '<span class="edit-link">',
                                '</span>'
                                );
                                ?>
                            </footer><!-- .entry-footer -->
                        <?php endif; ?>
                    </div>
                </article><!-- #post-## -->
            </div>
        </div>
        
        <?php if (get_field('who_we_serve')): ?>
        <div class="who">
            <div class="container">
                <div class="intro-text has-text-centered">
                    <?php echo get_field("who_we_serve"); ?>
                </div>
                <div class="columns">
                <div class="column is-half-tablet is-one-third-desktop">
                    <div class="feature" style="background-image: url('<?php echo get_field('flipcard_image'); ?>');">
                        <div class="overlay">
                            <div class="front">
                                <?php echo get_field('flip_front'); ?>
                            </div>
                            <div class="back">
                                <?php echo get_field('flip_back'); ?>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="column is-half-tablet is-one-third-desktop">
                    <div class="feature" style="background-image: url('<?php echo get_field('flipcard_image2'); ?>');">
                        <div class="overlay">
                            <div class="front">
                                <?php echo get_field('flip_front2'); ?>
                            </div>
                            <div class="back">
                                <?php echo get_field('flip_back2'); ?>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="column is-half-tablet is-one-third-desktop">
                    <div class="feature" style="background-image: url('<?php echo get_field('flipcard_image3'); ?>');">
                        <div class="overlay">
                            <div class="front">
                                <?php echo get_field('flip_front3'); ?>
                            </div>
                            <div class="back">
                                <?php echo get_field('flip_back3'); ?>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="column is-half-tablet is-one-third-desktop">
                    <div class="feature" style="background-image: url('<?php echo get_field('flipcard_image4'); ?>');">
                        <div class="overlay">
                            <div class="front">
                                <?php echo get_field('flip_front4'); ?>
                            </div>
                            <div class="back">
                                <?php echo get_field('flip_back4'); ?>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="column is-half-tablet is-one-third-desktop">
                    <div class="feature" style="background-image: url('<?php echo get_field('flipcard_image5'); ?>');">
                        <div class="overlay">
                            <div class="front">
                                <?php echo get_field('flip_front5'); ?>
                            </div>
                            <div class="back">
                                <?php echo get_field('flip_back5'); ?>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="column is-half-tablet is-one-third-desktop">
                    <div class="feature" style="background-image: url('<?php echo get_field('flipcard_image6'); ?>');">
                        <div class="overlay">
                            <div class="front">
                                <?php echo get_field('flip_front6'); ?>
                            </div>
                            <div class="back">
                                <?php echo get_field('flip_back6'); ?>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="column is-half-tablet is-one-third-desktop">
                    <div class="feature" style="background-image: url('<?php echo get_field('flipcard_image7'); ?>');">
                        <div class="overlay">
                            <div class="front">
                                <?php echo get_field('flip_front7'); ?>
                            </div>
                            <div class="back">
                                <?php echo get_field('flip_back7'); ?>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="column is-half-tablet is-one-third-desktop">
                    <div class="feature" style="background-image: url('<?php echo get_field('flipcard_image8'); ?>');">
                        <div class="overlay">
                            <div class="front">
                                <?php echo get_field('flip_front8'); ?>
                            </div>
                            <div class="back">
                                <?php echo get_field('flip_back8'); ?>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="column is-half-tablet is-one-third-desktop">
                    <div class="feature" style="background-image: url('<?php echo get_field('flipcard_image9'); ?>');">
                        <div class="overlay">
                            <div class="front">
                                <?php echo get_field('flip_front9'); ?>
                            </div>
                            <div class="back">
                                <?php echo get_field('flip_back9'); ?>
                            </div>
                        </div>
                    </div>
                </div>
                </div>
            </div>
        </div>
        <?php endif; ?>
        
        <div class="solutions">
            <div class="container">
                <div class="solutions-text has-text-centered">
                    <?php echo get_field('solutions'); ?>
                </div>
                <?php if (get_field('block_1_text') ): ?>
                <div class="columns">
                    <div onclick="location.href='<?php echo get_field('url'); ?>';" class="column is-half-tablet is-one-third-desktop">
                        <div class="img" style="background-image: url('<?php echo get_field('block_1_image'); ?>')"></div>
                        <div class="text">
                            <?php echo get_field('block_1_text'); ?>
                        </div>
                    </div>
                    <div onclick="location.href='<?php echo get_field('url2'); ?>';" class="column is-half-tablet is-one-third-desktop">
                        <div class="img" style="background-image: url('<?php echo get_field('block_2_image'); ?>')"></div>
                        <div class="text">
                            <?php echo get_field('block_2_text'); ?>
                        </div>
                    </div>
                    <div onclick="location.href='<?php echo get_field('url3'); ?>';" class="column is-half-tablet is-one-third-desktop">
                        <div class="img" style="background-image: url('<?php echo get_field('block_3_image'); ?>')"></div>
                        <div class="text">
                            <?php echo get_field('block_3_text'); ?>
                        </div>
                    </div>
                    <div onclick="location.href='<?php echo get_field('url4'); ?>';" class="column is-half-tablet is-one-third-desktop">
                        <div class="img" style="background-image: url('<?php echo get_field('block_4_image'); ?>')"></div>
                        <div class="text">
                            <?php echo get_field('block_4_text'); ?>
                        </div>
                    </div>
                    <div onclick="location.href='<?php echo get_field('url5'); ?>';" class="column is-half-tablet is-one-third-desktop">
                        <div class="img" style="background-image: url('<?php echo get_field('block_5_image'); ?>')"></div>
                        <div class="text">
                            <?php echo get_field('block_5_text'); ?>
                        </div>
                    </div>
                    <div onclick="location.href='<?php echo get_field('url6'); ?>';" class="column is-half-tablet is-one-third-desktop">
                        <div class="img" style="background-image: url('<?php echo get_field('block_6_image'); ?>')"></div>
                        <div class="text">
                            <?php echo get_field('block_6_text'); ?>
                        </div>
                    </div>
                </div>
                <?php echo get_field('speed_bottom_text'); ?>
                <?php endif; ?>
            </div>
        </div>
