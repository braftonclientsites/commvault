<?php
/**
 * Template part for displaying content
 *
 * @link https://codex.wordpress.org/Template_Hierarchy
 *
 * @package Bulmapress
 */
?>

<?php 
if (function_exists('has_post_thumbnail')) {
    if ( has_post_thumbnail() ) {
        $post_image_id = get_post_thumbnail_id($post_to_use->ID);
            if ($post_image_id) {
                $fthumb = wp_get_attachment_image_src( $post_image_id, 'large', false);
                if ($fthumb) (string)$fthumb = $fthumb[0];
            }
    }
} ?>

<article id="post-<?php the_ID(); ?>" <?php post_class(array('section', 'expertise')); ?>>
<div class="content container">
    <div class="background" style="background-image: url('<?php echo $fthumb; ?>');">
        <div class="overlay has-text-centered">
            <header class="entry-header">
                <?php the_title( '<h1 class="title is-1 entry-title">', '</h1>' ); ?>
            </header><!-- .entry-header -->

            <div class="content entry-content">
                <?php the_content();?>

                <?php wp_link_pages( array(
                    'before' => '<div class="page-links level">' . esc_html__( 'Pages:', 'bulmapress' ),
                    'after'  => '</div>',
                    ) ); ?>

                </div><!-- .entry-content -->

                <?php if ( get_edit_post_link() ) : ?>
                    <footer class="entry-footer">
                        <?php
                        edit_post_link(
                            sprintf(
                                /* translators: %s: Name of current post */
                                esc_html__( 'Edit %s', 'bulmapress' ),
                                the_title( '<span class="screen-reader-text">"', '"</span>', false )
                                ),
                            '<span class="edit-link">',
                            '</span>'
                            );
                            ?>
                        </footer><!-- .entry-footer -->
                    <?php endif; ?>
                </div>
            </div>
        </div>
    </article><!-- #post-## -->

	<div class="main expertise">
		<div class="container">
			<div class="first has-text-centered">
                <?php echo get_field('main_content'); ?>
                <div class="columns">
                    <div class="column is-one-third-tablet is-one-third-desktop has-text-centered"> 
                        <div class="img" style="background-image: url('<?php echo get_field('col_1_image')?>')"></div>
                        <?php echo get_field('col_1_content'); ?>
                    </div>
                    <div class="column is-one-third-tablet is-one-third-desktop has-text-centered"> 
                        <div class="img" style="background-image: url('<?php echo get_field('col_2_image')?>')"></div>
                        <?php echo get_field('col_2_content'); ?>
                    </div>
                    <div class="column is-one-third-tablet is-one-third-desktop has-text-centered"> 
                        <div class="img" style="background-image: url('<?php echo get_field('col_3_image')?>')"></div>
                        <?php echo get_field('col_3_content'); ?>
                    </div>
                </div>
                <?php echo get_field('main_bottom'); ?>
            </div>
        </div>
    </div>
	<?php if (get_field('secondary_content') ) : ?>
	<div class="second">
		<div class="container">
            <?php echo get_field('secondary_content'); ?>
        </div>
	</div>
	<?php endif; ?>
	<?php if (get_field('tertiary_content') ) : ?>
	<div class="third">
        <div class="container">
            <?php echo get_field('tertiary_content'); ?>
            <div class="columns">
                <div class="column">
                    <?php echo get_field('result1'); ?>
                </div>
                <div class="column">
                    <?php echo get_field('result2'); ?>
                </div>
                <div class="column">
                    <?php echo get_field('result3'); ?>
                </div>
                <div class="column">
                    <?php echo get_field('result4'); ?>
                </div>
                <div class="column">
                    <?php echo get_field('result5'); ?>
                </div>
            </div>
            <div class="has-text-centered">
                <?php echo get_field('cta'); ?>
            </div>
        </div>
    </div>
	<?php endif; ?>

    <div class="subnav-buttons">
        <div class="container has-text-centered">
            <?php echo wp_list_pages( 'sort_column=menu_order&title_li=&child_of=6'); ?>
        </div>
    </div>
</div>
