<?php
if(function_exists("register_field_group"))
{
	register_field_group(array (
		'id' => 'acf_homepage',
		'title' => 'Homepage',
		'fields' => array (
			array (
				'key' => 'field_599b007827d68',
				'label' => 'Feature 1 Text',
				'name' => 'feature_1_text',
				'type' => 'wysiwyg',
				'default_value' => '',
				'toolbar' => 'full',
				'media_upload' => 'yes',
			),
			array (
				'key' => 'field_599b007827d68back',
				'label' => 'Feature 1 Back Text',
				'name' => 'feature_1_back',
				'type' => 'wysiwyg',
				'default_value' => '',
				'toolbar' => 'full',
				'media_upload' => 'yes',
			),
			array (
				'key' => 'field_599b008327d69',
				'label' => 'Feature 1 Image',
				'name' => 'feature_1_image',
				'type' => 'image',
				'save_format' => 'url',
				'preview_size' => 'thumbnail',
				'library' => 'all',
			),
			array (
				'key' => 'field_599b008b27d6a',
				'label' => 'Feature 2 Text',
				'name' => 'feature_2_text',
				'type' => 'wysiwyg',
				'default_value' => '',
				'toolbar' => 'full',
				'media_upload' => 'yes',
			),
			array (
				'key' => 'field_599b008b27d6aback',
				'label' => 'Feature 2 Back Text',
				'name' => 'feature_2_back',
				'type' => 'wysiwyg',
				'default_value' => '',
				'toolbar' => 'full',
				'media_upload' => 'yes',
			),
			array (
				'key' => 'field_599b009127d6b',
				'label' => 'Feature 2 Image',
				'name' => 'feature_2_image',
				'type' => 'image',
				'save_format' => 'url',
				'preview_size' => 'thumbnail',
				'library' => 'all',
			),
			array (
				'key' => 'field_599b009a27d6c',
				'label' => 'Feature 3 Text',
				'name' => 'feature_3_text',
				'type' => 'wysiwyg',
				'default_value' => '',
				'toolbar' => 'full',
				'media_upload' => 'yes',
			),
			array (
				'key' => 'field_599b009a27d6cback',
				'label' => 'Feature 3 Back Text',
				'name' => 'feature_3_back',
				'type' => 'wysiwyg',
				'default_value' => '',
				'toolbar' => 'full',
				'media_upload' => 'yes',
			),
			array (
				'key' => 'field_599b00a127d6d',
				'label' => 'Feature 3 Image',
				'name' => 'feature_3_image',
				'type' => 'image',
				'save_format' => 'url',
				'preview_size' => 'thumbnail',
				'library' => 'all',
			),
			array (
				'key' => 'field_599b00a927d6e',
				'label' => 'Feature 4 Text',
				'name' => 'feature_4_text',
				'type' => 'wysiwyg',
				'default_value' => '',
				'toolbar' => 'full',
				'media_upload' => 'yes',
			),
			array (
				'key' => 'field_599b00a927d6eback',
				'label' => 'Feature 4 Back Text',
				'name' => 'feature_4_back',
				'type' => 'wysiwyg',
				'default_value' => '',
				'toolbar' => 'full',
				'media_upload' => 'yes',
			),
			array (
				'key' => 'field_599b00b127d6f',
				'label' => 'Feature 4 Image',
				'name' => 'feature_4_image',
				'type' => 'image',
				'save_format' => 'url',
				'preview_size' => 'thumbnail',
				'library' => 'all',
			),
			array (
				'key' => 'field_599b00a927d6e5',
				'label' => 'Feature 5 Text',
				'name' => 'feature_5_text',
				'type' => 'wysiwyg',
				'default_value' => '',
				'toolbar' => 'full',
				'media_upload' => 'yes',
			),
			array (
				'key' => 'field_599b00a927d6eback5',
				'label' => 'Feature 5 Back Text',
				'name' => 'feature_5_back',
				'type' => 'wysiwyg',
				'default_value' => '',
				'toolbar' => 'full',
				'media_upload' => 'yes',
			),
			array (
				'key' => 'field_599b00b127d6f5',
				'label' => 'Feature 5 Image',
				'name' => 'feature_5_image',
				'type' => 'image',
				'save_format' => 'url',
				'preview_size' => 'thumbnail',
				'library' => 'all',
			),
			array (
				'key' => 'field_599b00a927d6e6',
				'label' => 'Feature 6 Text',
				'name' => 'feature_6_text',
				'type' => 'wysiwyg',
				'default_value' => '',
				'toolbar' => 'full',
				'media_upload' => 'yes',
			),
			array (
				'key' => 'field_599b00a927d6eback6',
				'label' => 'Feature 6 Back Text',
				'name' => 'feature_6_back',
				'type' => 'wysiwyg',
				'default_value' => '',
				'toolbar' => 'full',
				'media_upload' => 'yes',
			),
			array (
				'key' => 'field_599b00b127d6f6',
				'label' => 'Feature 6 Image',
				'name' => 'feature_6_image',
				'type' => 'image',
				'save_format' => 'url',
				'preview_size' => 'thumbnail',
				'library' => 'all',
			),
			array (
				'key' => 'field_599b00ce27d71',
				'label' => 'Our Company Text',
				'name' => 'company',
				'type' => 'wysiwyg',
				'default_value' => '',
				'toolbar' => 'full',
				'media_upload' => 'yes',
			),
		),
		'location' => array (
			array (
				array (
					'param' => 'page',
					'operator' => '==',
					'value' => '2',
					'order_no' => 0,
					'group_no' => 0,
				),
			),
			array (
				array (
					'param' => 'page',
					'operator' => '==',
					'value' => '4',
					'order_no' => 0,
					'group_no' => 0,
				),
			),
		),
		'options' => array (
			'position' => 'normal',
			'layout' => 'no_box',
			'hide_on_screen' => array (
			),
		),
		'menu_order' => 0,
	));
}
