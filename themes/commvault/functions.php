<?php
/**
 * Bulmapress functions and definitions
 *
 * @link https://developer.wordpress.org/themes/basics/theme-functions/
 *
 * @package Bulmapress
 */
 
// add custom fields
include_once get_template_directory().'/custom-fields/fields.php';

require get_template_directory() . '/functions/bulmapress_navwalker.php';
require get_template_directory() . '/functions/bulmapress_helpers.php';
require get_template_directory() . '/functions/bulmapress_custom_query.php';

if ( ! function_exists( 'bulmapress_setup' ) ) :
/**
 * Sets up theme defaults and registers support for various WordPress features.
 *
 * Note that this function is hooked into the after_setup_theme hook, which
 * runs before the init hook. The init hook is too late for some features, such
 * as indicating support for post thumbnails.
 */
function bulmapress_setup() {
	require get_template_directory() . '/functions/base.php';
	require get_template_directory() . '/functions/post-thumbnails.php';
	require get_template_directory() . '/functions/navigation.php';
	require get_template_directory() . '/functions/content.php';
	require get_template_directory() . '/functions/pagination.php';
	require get_template_directory() . '/functions/widgets.php';
	require get_template_directory() . '/functions/search.php';
	require get_template_directory() . '/functions/scripts-styles.php';
}
endif;
add_action( 'after_setup_theme', 'bulmapress_setup' );

require get_template_directory() . '/functions/template-tags.php';
require get_template_directory() . '/functions/extras.php';
require get_template_directory() . '/functions/customizer.php';
require get_template_directory() . '/functions/jetpack.php';

function custom_post_example() { 
	// creating (registering) the custom type 
	register_post_type( 'testimonials', /* (http://codex.wordpress.org/Function_Reference/register_post_type) */
		// let's now add all the options for this post type
		array( 'labels' => array(
			'name' => __( 'Testimonials', 'branchserv' ), /* This is the Title of the Group */
			'singular_name' => __( 'Testimonial', 'branchserv' ), /* This is the individual type */
			'all_items' => __( 'All Testimonials', 'branchserv' ), /* the all items menu item */
			'add_new' => __( 'Add New', 'branchserv' ), /* The add new menu item */
			'add_new_item' => __( 'Add New Testimonial', 'branchserv' ), /* Add New Display Title */
			'edit' => __( 'Edit', 'branchserv' ), /* Edit Dialog */
			'edit_item' => __( 'Edit Testimonial', 'branchserv' ), /* Edit Display Title */
			'new_item' => __( 'New Testimonial', 'branchserv' ), /* New Display Title */
			'view_item' => __( 'View Testimonial', 'branchserv' ), /* View Display Title */
			'search_items' => __( 'Search Testimonials', 'branchserv' ), /* Search Custom Type Title */ 
			'not_found' =>  __( 'Nothing found in the Database.', 'branchserv' ), /* This displays if there are no entries yet */ 
			'not_found_in_trash' => __( 'Nothing found in Trash', 'branchserv' ), /* This displays if there is nothing in the trash */
			'parent_item_colon' => ''
			), /* end of arrays */
			'description' => __( 'This is the testimonials custom post type', 'branchserv' ), /* Custom Type Description */
			'public' => true,
			'publicly_queryable' => true,
			'exclude_from_search' => false,
			'show_ui' => true,
			'query_var' => true,
			'menu_position' => 8, /* this is what order you want it to appear in on the left hand side menu */ 
			'menu_icon' => get_stylesheet_directory_uri() . '/frontend/images/custom-post-icon.png', /* the icon for the custom post type menu */
			'rewrite'	=> array( 'slug' => 'testimonials', 'with_front' => false ), /* you can specify its url slug */
			'has_archive' => 'testimonials', /* you can rename the slug here */
			'capability_type' => 'post',
			'hierarchical' => false,
			/* the next one is important, it tells what's enabled in the post editor */
			'supports' => array( 'title', 'editor', 'author', 'thumbnail', 'excerpt', 'trackbacks', 'custom-fields', 'comments', 'revisions', 'sticky')
		) /* end of options */
	); /* end of register post type */

	register_post_type( 'team', /* (http://codex.wordpress.org/Function_Reference/register_post_type) */
		// let's now add all the options for this post type
		array( 'labels' => array(
			'name' => __( 'Team Members', 'branchserv' ), /* This is the Title of the Group */
			'singular_name' => __( 'Team Member', 'branchserv' ), /* This is the individual type */
			'all_items' => __( 'All Team Members', 'branchserv' ), /* the all items menu item */
			'add_new' => __( 'Add New', 'branchserv' ), /* The add new menu item */
			'add_new_item' => __( 'Add New Team Member', 'branchserv' ), /* Add New Display Title */
			'edit' => __( 'Edit', 'branchserv' ), /* Edit Dialog */
			'edit_item' => __( 'Edit Team Member', 'branchserv' ), /* Edit Display Title */
			'new_item' => __( 'New Team Member', 'branchserv' ), /* New Display Title */
			'view_item' => __( 'View Team Member', 'branchserv' ), /* View Display Title */
			'search_items' => __( 'Search Team Member', 'branchserv' ), /* Search Custom Type Title */ 
			'not_found' =>  __( 'Nothing found in the Database.', 'branchserv' ), /* This displays if there are no entries yet */ 
			'not_found_in_trash' => __( 'Nothing found in Trash', 'branchserv' ), /* This displays if there is nothing in the trash */
			'parent_item_colon' => ''
			), /* end of arrays */
			'description' => __( 'This is the team custom post type', 'branchserv' ), /* Custom Type Description */
			'public' => true,
			'publicly_queryable' => true,
			'exclude_from_search' => false,
			'show_ui' => true,
			'query_var' => true,
			'menu_position' => 8, /* this is what order you want it to appear in on the left hand side menu */ 
			'menu_icon' => get_stylesheet_directory_uri() . '/frontend/images/custom-post-icon.png', /* the icon for the custom post type menu */
			'rewrite'	=> array( 'slug' => 'team-members', 'with_front' => false ), /* you can specify its url slug */
			'has_archive' => 'team-members', /* you can rename the slug here */
			'capability_type' => 'post',
			'hierarchical' => false,
			/* the next one is important, it tells what's enabled in the post editor */
			'supports' => array( 'title', 'editor', 'author', 'thumbnail', 'excerpt', 'trackbacks', 'custom-fields', 'comments', 'revisions', 'sticky')
		) /* end of options */
	); /* end of register post type */

	register_post_type( 'news', /* (http://codex.wordpress.org/Function_Reference/register_post_type) */
		// let's now add all the options for this post type
		array( 'labels' => array(
			'name' => __( 'News', 'branchserv' ), /* This is the Title of the Group */
			'singular_name' => __( 'Item', 'branchserv' ), /* This is the individual type */
			'all_items' => __( 'All Items', 'branchserv' ), /* the all items menu item */
			'add_new' => __( 'Add New', 'branchserv' ), /* The add new menu item */
			'add_new_item' => __( 'Add New Item', 'branchserv' ), /* Add New Display Title */
			'edit' => __( 'Edit', 'branchserv' ), /* Edit Dialog */
			'edit_item' => __( 'Edit Item', 'branchserv' ), /* Edit Display Title */
			'new_item' => __( 'New Item', 'branchserv' ), /* New Display Title */
			'view_item' => __( 'View Item', 'branchserv' ), /* View Display Title */
			'search_items' => __( 'Search Item', 'branchserv' ), /* Search Custom Type Title */ 
			'not_found' =>  __( 'Nothing found in the Database.', 'branchserv' ), /* This displays if there are no entries yet */ 
			'not_found_in_trash' => __( 'Nothing found in Trash', 'branchserv' ), /* This displays if there is nothing in the trash */
			'parent_item_colon' => ''
			), /* end of arrays */
			'description' => __( 'This is the events custom post type', 'branchserv' ), /* Custom Type Description */
			'public' => true,
			'publicly_queryable' => true,
			'exclude_from_search' => false,
			'show_ui' => true,
			'query_var' => true,
			'menu_position' => 8, /* this is what order you want it to appear in on the left hand side menu */ 
			'menu_icon' => get_stylesheet_directory_uri() . '/frontend/images/custom-post-icon.png', /* the icon for the custom post type menu */
			'rewrite'	=> array( 'slug' => 'news', 'with_front' => false ), /* you can specify its url slug */
			'has_archive' => 'news', /* you can rename the slug here */
			'capability_type' => 'post',
			'hierarchical' => false,
			/* the next one is important, it tells what's enabled in the post editor */
			'supports' => array( 'title', 'editor', 'author', 'thumbnail', 'excerpt', 'trackbacks', 'custom-fields', 'comments', 'revisions', 'sticky')
		) /* end of options */
	); /* end of register post type */

	register_post_type( 'events', /* (http://codex.wordpress.org/Function_Reference/register_post_type) */
		// let's now add all the options for this post type
		array( 'labels' => array(
			'name' => __( 'Events', 'branchserv' ), /* This is the Title of the Group */
			'singular_name' => __( 'Item', 'branchserv' ), /* This is the individual type */
			'all_items' => __( 'All Items', 'branchserv' ), /* the all items menu item */
			'add_new' => __( 'Add New', 'branchserv' ), /* The add new menu item */
			'add_new_item' => __( 'Add New Item', 'branchserv' ), /* Add New Display Title */
			'edit' => __( 'Edit', 'branchserv' ), /* Edit Dialog */
			'edit_item' => __( 'Edit Item', 'branchserv' ), /* Edit Display Title */
			'new_item' => __( 'New Item', 'branchserv' ), /* New Display Title */
			'view_item' => __( 'View Item', 'branchserv' ), /* View Display Title */
			'search_items' => __( 'Search Item', 'branchserv' ), /* Search Custom Type Title */ 
			'not_found' =>  __( 'Nothing found in the Database.', 'branchserv' ), /* This displays if there are no entries yet */ 
			'not_found_in_trash' => __( 'Nothing found in Trash', 'branchserv' ), /* This displays if there is nothing in the trash */
			'parent_item_colon' => ''
			), /* end of arrays */
			'description' => __( 'This is the events custom post type', 'branchserv' ), /* Custom Type Description */
			'public' => true,
			'publicly_queryable' => true,
			'exclude_from_search' => false,
			'show_ui' => true,
			'query_var' => true,
			'menu_position' => 8, /* this is what order you want it to appear in on the left hand side menu */ 
			'menu_icon' => get_stylesheet_directory_uri() . '/frontend/images/custom-post-icon.png', /* the icon for the custom post type menu */
			'rewrite'	=> array( 'slug' => 'events', 'with_front' => false ), /* you can specify its url slug */
			'has_archive' => 'events', /* you can rename the slug here */
			'capability_type' => 'post',
			'hierarchical' => false,
			/* the next one is important, it tells what's enabled in the post editor */
			'supports' => array( 'title', 'editor', 'author', 'thumbnail', 'excerpt', 'trackbacks', 'custom-fields', 'comments', 'revisions', 'sticky')
		) /* end of options */
	); /* end of register post type */

	register_post_type( 'careers', /* (http://codex.wordpress.org/Function_Reference/register_post_type) */
	// let's now add all the options for this post type
	array( 'labels' => array(
		'name' => __( 'Careers', 'branchserv' ), /* This is the Title of the Group */
		'singular_name' => __( 'Item', 'branchserv' ), /* This is the individual type */
		'all_items' => __( 'All Items', 'branchserv' ), /* the all items menu item */
		'add_new' => __( 'Add New', 'branchserv' ), /* The add new menu item */
		'add_new_item' => __( 'Add New Item', 'branchserv' ), /* Add New Display Title */
		'edit' => __( 'Edit', 'branchserv' ), /* Edit Dialog */
		'edit_item' => __( 'Edit Item', 'branchserv' ), /* Edit Display Title */
		'new_item' => __( 'New Item', 'branchserv' ), /* New Display Title */
		'view_item' => __( 'View Item', 'branchserv' ), /* View Display Title */
		'search_items' => __( 'Search Item', 'branchserv' ), /* Search Custom Type Title */ 
		'not_found' =>  __( 'Nothing found in the Database.', 'branchserv' ), /* This displays if there are no entries yet */ 
		'not_found_in_trash' => __( 'Nothing found in Trash', 'branchserv' ), /* This displays if there is nothing in the trash */
		'parent_item_colon' => ''
		), /* end of arrays */
		'description' => __( 'This is the careers custom post type', 'branchserv' ), /* Custom Type Description */
		'public' => true,
		'publicly_queryable' => true,
		'exclude_from_search' => false,
		'show_ui' => true,
		'query_var' => true,
		'menu_position' => 8, /* this is what order you want it to appear in on the left hand side menu */ 
		'menu_icon' => get_stylesheet_directory_uri() . '/frontend/images/custom-post-icon.png', /* the icon for the custom post type menu */
		'rewrite'	=> array( 'slug' => 'careers', 'with_front' => false ), /* you can specify its url slug */
		'has_archive' => 'careers', /* you can rename the slug here */
		'capability_type' => 'post',
		'hierarchical' => false,
		/* the next one is important, it tells what's enabled in the post editor */
		'supports' => array( 'title', 'editor', 'author', 'thumbnail', 'excerpt', 'trackbacks', 'custom-fields', 'comments', 'revisions', 'sticky')
		) /* end of options */
	); /* end of register post type */

	register_post_type( 'products', /* (http://codex.wordpress.org/Function_Reference/register_post_type) */
	// let's now add all the options for this post type
	array( 'labels' => array(
		'name' => __( 'Products', 'branchserv' ), /* This is the Title of the Group */
		'singular_name' => __( 'Product', 'branchserv' ), /* This is the individual type */
		'all_items' => __( 'All Items', 'branchserv' ), /* the all items menu item */
		'add_new' => __( 'Add New', 'branchserv' ), /* The add new menu item */
		'add_new_item' => __( 'Add New Item', 'branchserv' ), /* Add New Display Title */
		'edit' => __( 'Edit', 'branchserv' ), /* Edit Dialog */
		'edit_item' => __( 'Edit Item', 'branchserv' ), /* Edit Display Title */
		'new_item' => __( 'New Item', 'branchserv' ), /* New Display Title */
		'view_item' => __( 'View Item', 'branchserv' ), /* View Display Title */
		'search_items' => __( 'Search Item', 'branchserv' ), /* Search Custom Type Title */ 
		'not_found' =>  __( 'Nothing found in the Database.', 'branchserv' ), /* This displays if there are no entries yet */ 
		'not_found_in_trash' => __( 'Nothing found in Trash', 'branchserv' ), /* This displays if there is nothing in the trash */
		'parent_item_colon' => ''
		), /* end of arrays */
		'description' => __( 'This is the products custom post type', 'branchserv' ), /* Custom Type Description */
		'public' => true,
		'publicly_queryable' => true,
		'exclude_from_search' => false,
		'show_ui' => true,
		'query_var' => true,
		'menu_position' => 8, /* this is what order you want it to appear in on the left hand side menu */ 
		'menu_icon' => get_stylesheet_directory_uri() . '/frontend/images/custom-post-icon.png', /* the icon for the custom post type menu */
		'rewrite'	=> array( 'slug' => 'products', 'with_front' => false ), /* you can specify its url slug */
		'has_archive' => 'products', /* you can rename the slug here */
		'capability_type' => 'post',
		'hierarchical' => false,
		/* the next one is important, it tells what's enabled in the post editor */
		'supports' => array( 'title', 'editor', 'author', 'thumbnail', 'excerpt', 'trackbacks', 'custom-fields', 'comments', 'revisions', 'sticky')
		) /* end of options */
	); /* end of register post type */
	
	register_post_type( 'fact-sheets', /* (http://codex.wordpress.org/Function_Reference/register_post_type) */
	// let's now add all the options for this post type
	array( 'labels' => array(
		'name' => __( 'Fact Sheets', 'branchserv' ), /* This is the Title of the Group */
		'singular_name' => __( 'Fact Sheet', 'branchserv' ), /* This is the individual type */
		'all_items' => __( 'All Items', 'branchserv' ), /* the all items menu item */
		'add_new' => __( 'Add New', 'branchserv' ), /* The add new menu item */
		'add_new_item' => __( 'Add New Item', 'branchserv' ), /* Add New Display Title */
		'edit' => __( 'Edit', 'branchserv' ), /* Edit Dialog */
		'edit_item' => __( 'Edit Item', 'branchserv' ), /* Edit Display Title */
		'new_item' => __( 'New Item', 'branchserv' ), /* New Display Title */
		'view_item' => __( 'View Item', 'branchserv' ), /* View Display Title */
		'search_items' => __( 'Search Item', 'branchserv' ), /* Search Custom Type Title */ 
		'not_found' =>  __( 'Nothing found in the Database.', 'branchserv' ), /* This displays if there are no entries yet */ 
		'not_found_in_trash' => __( 'Nothing found in Trash', 'branchserv' ), /* This displays if there is nothing in the trash */
		'parent_item_colon' => ''
		), /* end of arrays */
		'description' => __( 'This is the fact sheets custom post type', 'branchserv' ), /* Custom Type Description */
		'public' => true,
		'publicly_queryable' => true,
		'exclude_from_search' => false,
		'show_ui' => true,
		'query_var' => true,
		'menu_position' => 8, /* this is what order you want it to appear in on the left hand side menu */ 
		'menu_icon' => get_stylesheet_directory_uri() . '/frontend/images/custom-post-icon.png', /* the icon for the custom post type menu */
		'rewrite'	=> array( 'slug' => 'fact-sheets', 'with_front' => false ), /* you can specify its url slug */
		'has_archive' => 'fact-sheets', /* you can rename the slug here */
		'capability_type' => 'post',
		'hierarchical' => false,
		/* the next one is important, it tells what's enabled in the post editor */
		'supports' => array( 'title', 'editor', 'author', 'thumbnail', 'excerpt', 'trackbacks', 'custom-fields', 'comments', 'revisions', 'sticky')
	) /* end of options */
); /* end of register post type */
}

	// adding the function to the Wordpress init
	add_action( 'init', 'custom_post_example');
	
	/*
	for more information on taxonomies, go here:
	http://codex.wordpress.org/Function_Reference/register_taxonomy
	*/
	
	// now let's add custom categories (these act like categories)
	register_taxonomy( 'custom_cat', 
		array('testimonials'), /* if you change the name of register_post_type( 'custom_type', then you have to change this */
		array('hierarchical' => true,     /* if this is true, it acts like categories */
			'labels' => array(
				'name' => __( 'Testimonial Categories', 'branchserv' ), /* name of the custom taxonomy */
				'singular_name' => __( 'Testimonial Category', 'branchserv' ), /* single taxonomy name */
				'search_items' =>  __( 'Search Testimonial Categories', 'branchserv' ), /* search title for taxomony */
				'all_items' => __( 'All Testimonial Categories', 'branchserv' ), /* all title for taxonomies */
				'parent_item' => __( 'Parent Testimonial Category', 'branchserv' ), /* parent title for taxonomy */
				'parent_item_colon' => __( 'Parent Testimonial Category:', 'branchserv' ), /* parent taxonomy title */
				'edit_item' => __( 'Edit Testimonial Category', 'branchserv' ), /* edit custom taxonomy title */
				'update_item' => __( 'Update Testimonial Category', 'branchserv' ), /* update title for taxonomy */
				'add_new_item' => __( 'Add New Testimonial Category', 'branchserv' ), /* add new title for taxonomy */
				'new_item_name' => __( 'New Testimonial Category Name', 'branchserv' ) /* name title for taxonomy */
			),
			'show_admin_column' => true, 
			'show_ui' => true,
			'query_var' => true,
			'rewrite' => array( 'slug' => 'testimonial-type' ),
		)
	);

	register_taxonomy( 'custom_career_cat', 
		array('careers'), /* if you change the name of register_post_type( 'custom_type', then you have to change this */
		array('hierarchical' => true,     /* if this is true, it acts like categories */
			'labels' => array(
				'name' => __( 'Career Categories', 'branchserv' ), /* name of the custom taxonomy */
				'singular_name' => __( 'Career Category', 'branchserv' ), /* single taxonomy name */
				'search_items' =>  __( 'Search Career Categories', 'branchserv' ), /* search title for taxomony */
				'all_items' => __( 'All Career Categories', 'branchserv' ), /* all title for taxonomies */
				'parent_item' => __( 'Parent Career Category', 'branchserv' ), /* parent title for taxonomy */
				'parent_item_colon' => __( 'Parent Career Category:', 'branchserv' ), /* parent taxonomy title */
				'edit_item' => __( 'Edit Career Category', 'branchserv' ), /* edit custom taxonomy title */
				'update_item' => __( 'Update Career Category', 'branchserv' ), /* update title for taxonomy */
				'add_new_item' => __( 'Add New Career Category', 'branchserv' ), /* add new title for taxonomy */
				'new_item_name' => __( 'New Career Category Name', 'branchserv' ) /* name title for taxonomy */
			),
			'show_admin_column' => true, 
			'show_ui' => true,
			'query_var' => true,
			'rewrite' => array( 'slug' => 'career-type' ),
		)
	);

	register_taxonomy( 'custom_product_cat', 
		array('products'), /* if you change the name of register_post_type( 'custom_type', then you have to change this */
		array('hierarchical' => true,     /* if this is true, it acts like categories */
			'labels' => array(
				'name' => __( 'Product Categories', 'branchserv' ), /* name of the custom taxonomy */
				'singular_name' => __( 'Product Category', 'branchserv' ), /* single taxonomy name */
				'search_items' =>  __( 'Search Product Categories', 'branchserv' ), /* search title for taxomony */
				'all_items' => __( 'All Product Categories', 'branchserv' ), /* all title for taxonomies */
				'parent_item' => __( 'Parent Product Category', 'branchserv' ), /* parent title for taxonomy */
				'parent_item_colon' => __( 'Parent Product Category:', 'branchserv' ), /* parent taxonomy title */
				'edit_item' => __( 'Edit Product Category', 'branchserv' ), /* edit custom taxonomy title */
				'update_item' => __( 'Update Product Category', 'branchserv' ), /* update title for taxonomy */
				'add_new_item' => __( 'Add New Product Category', 'branchserv' ), /* add new title for taxonomy */
				'new_item_name' => __( 'New Product Category Name', 'branchserv' ) /* name title for taxonomy */
			),
			'show_admin_column' => true, 
			'show_ui' => true,
			'query_var' => true,
			'rewrite' => array( 'slug' => 'product-type' ),
		)
	);

	register_taxonomy( 'custom_fact_sheet_cat', 
		array('fact-sheets'), /* if you change the name of register_post_type( 'custom_type', then you have to change this */
		array('hierarchical' => true,     /* if this is true, it acts like categories */
			'labels' => array(
				'name' => __( 'Fact Sheet Categories', 'branchserv' ), /* name of the custom taxonomy */
				'singular_name' => __( 'Fact Sheet Category', 'branchserv' ), /* single taxonomy name */
				'search_items' =>  __( 'Search Fact Sheet Categories', 'branchserv' ), /* search title for taxomony */
				'all_items' => __( 'All Fact Sheet Categories', 'branchserv' ), /* all title for taxonomies */
				'parent_item' => __( 'Parent Fact Sheet Category', 'branchserv' ), /* parent title for taxonomy */
				'parent_item_colon' => __( 'Parent Fact Sheet Category:', 'branchserv' ), /* parent taxonomy title */
				'edit_item' => __( 'Edit Fact Sheet Category', 'branchserv' ), /* edit custom taxonomy title */
				'update_item' => __( 'Update Fact Sheet Category', 'branchserv' ), /* update title for taxonomy */
				'add_new_item' => __( 'Add New Fact Sheet Category', 'branchserv' ), /* add new title for taxonomy */
				'new_item_name' => __( 'New Fact Sheet Category Name', 'branchserv' ) /* name title for taxonomy */
			),
			'show_admin_column' => true, 
			'show_ui' => true,
			'query_var' => true,
			'rewrite' => array( 'slug' => 'fact-sheet-type' ),
		)
	);

	function wpb_list_child_pages() { 
		
	   global $post; 
	   $current = $post->ID;
		
	   if ( is_page() && $post->post_parent )
		
		   $childpages = wp_list_pages( 'sort_column=menu_order&title_li=&child_of=' . $post->post_parent . '&echo=0&exclude=' . $current . '' );
	   else
		   $childpages = wp_list_pages( 'sort_column=menu_order&title_li=&child_of=' . $post->ID . '&echo=0&exclude=' . $current . '');
		
	   if ( $childpages ) {
		
		   $string = '<ul>' . $childpages . '</ul>';
	   }
		
	   return $string;
		
	   }
		
	add_shortcode('wpb_childpages', 'wpb_list_child_pages');

/**

 * Extend WordPress search to include custom fields

 *

 * http://adambalee.com

 */



/**

 * Join posts and postmeta tables

 *

 * http://codex.wordpress.org/Plugin_API/Filter_Reference/posts_join

 */

function cf_search_join( $join ) {

    global $wpdb;



    if ( is_search() ) {    

        $join .=' LEFT JOIN '.$wpdb->postmeta. ' ON '. $wpdb->posts . '.ID = ' . $wpdb->postmeta . '.post_id ';

    }

    

    return $join;

}

add_filter('posts_join', 'cf_search_join' );



/**

 * Modify the search query with posts_where

 *

 * http://codex.wordpress.org/Plugin_API/Filter_Reference/posts_where

 */

function cf_search_where( $where ) {

    global $wpdb;

   

    if ( is_search() ) {

        $where = preg_replace(

            "/\(\s*".$wpdb->posts.".post_title\s+LIKE\s*(\'[^\']+\')\s*\)/",

            "(".$wpdb->posts.".post_title LIKE $1) OR (".$wpdb->postmeta.".meta_value LIKE $1)", $where );

    }



    return $where;

}

add_filter( 'posts_where', 'cf_search_where' );



/**

 * Prevent duplicates

 *

 * http://codex.wordpress.org/Plugin_API/Filter_Reference/posts_distinct

 */

function cf_search_distinct( $where ) {

    global $wpdb;



    if ( is_search() ) {

        return "DISTINCT";

    }



    return $where;

}

add_filter( 'posts_distinct', 'cf_search_distinct' );