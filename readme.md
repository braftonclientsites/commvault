# Brafton Website Redeisgns
The following is instructions on the workflow for build clients sites.

## Step 1: Create the client repository

- Create a new repository on bitbucket for your new site build project
    - Owner: "braftonclientsites"
    - Project: "sites"
    - Respository Name: {ClientName} [note: There can be no spaces]
    - Respository type: Git 

## Step 2: Modify repository for client

- clone this repository ` git clone https://brafton@bitbucket.org/braftonclientsites/basebuild.git {clientName}` to your local computer
- Add required plugins to plugin folder
    - You repo already includes a set of 5 required plugins and some additional optional plugins. See the [Plugins Doc](plugins.md) for information around these plugins
- Add your base theme to the repo under the "themes" folder and remove an themes you will not be using
    - Your base includes the following themes for use as your base [Delete any you do not wish to use using `git rm themes/{themeName}`. IMPORTANT: You must do an add and commit after each removal of submodule themes `git add -A && git commit -m "remove {themeName}"`
        - Twentysixteen [ for use as a base ]
        - renameme [ empty child theme already linked to Twentysixteen ]
        - bones [ a great theme for building sites ]
        - expanse [ A Brafton developed theme by Yvonne Tse ] `git rm themes/expanse`
            - This is acatually a ["submodule"](https://github.com/blog/2104-working-with-submodules) which you can treat like a self contained git repo.  This has been added to use as a parent theme but if you need or want to make modifucations to the parent theme you can use this inner repo to be able to add/commit/push changes to the [Expase repository on git](https://github.com/BraftonSupport/expanse)
        - longscroll [ A Brafton developed theme by Yvonne Tse ] `git rm themes/longscroll`
            - This is acatually a ["submodule"](https://github.com/blog/2104-working-with-submodules) which you can treat like a self contained git repo.  This has been added to use as a parent theme but if you need or want to make modifucations to the parent theme you can use this inner repo to be able to add/commit/push changes to the [Longscroll repository on git](https://github.com/BraftonSupport/longscroll)
        - visual [ A Brafton developed theme by Gregory Rich ] `git rm themes/visual` 
            - This is acatually a ["submodule"](https://github.com/blog/2104-working-with-submodules) which you can treat like a self contained git repo.  This has been added to use as a parent theme but if you need or want to make modifucations to the parent theme you can use this inner repo to be able to add/commit/push changes to the [Visual repository on git](https://github.com/BraftonSupport/visual)
    - Once you have set your theme folders up you need to run `git submodule update --init --recursive`
- Modify the hermes.json file
    - name: The name of this project. The client Name is what is recommended
        - this field can only contain alpha numeric characters with no spaces.  Camel case is recommended.
    - repo: the name of the repository you created in Step 1
    - global.title: The title for your wordpress site. This can be changed later via the wordpress GUI. This is just used to create your wordpress site
    - global.theme: You development theme folder name
    - global.parent: If using a parent theme it must exist in themes folder. provide the folder name here
    - global.deploy_parent: ("y" or "n")
        - The first time you deploy your site to the design server your parent will be automatically deployed. If you make changes and need to deploy this theme later you can change the value to "y" (note this is lowercase) to deploy your parent theme again.
    - global.deploy_branch: The branch that will be used to deploy for your development
        - the default is "develop" so you will need to create a develop branch

                

                    #Creates the develop branch and push up to bitbucket
                    git checkout -b develop
                    git push origin develop
                    #Create a branch to do work from and merge into the develop branch
                    git checkout -b active-beta 
                    git push origin active-beta

                

    - global.deploy_plugins: ("y" or "n") This will deploy all plugins to the site
        - The first time you deploy your site the plugins will be deployed automatically and updated to their most up to date versions
        - The required 3 plugins during dev will be activated automatically for you
            - Brafton Content Importer
            - YOAST
            - WP Smushit
    - global.update_plugins: ("y" or "n") This will update all the plugins on the site
        - The first time you deploy your site all plugins will be automatically updated
    - global.package_site: ("y" or "n") 
        - This will create a zip file with the entire contents of the wp-content folder and a .sql file of the database with the new sites url
    - global.soft_launch_url: The url being used for soft launch. This will be required to properly populate the exported sql file

- Connect the new Folder to the repository [this assumes the folder/repo name is "myclient" and your username is "bradtest" ]
    - Rename basebuild to "myclient"
    - ` cd myclient`
    - ` git remote set-url origin https://bradtest@bitbucket.org/braftonclientsites/myclient`
    - ` git commit -m "first commit"`
    - ` git push origin master`

## Step : Setup local dev enviorment
Regardless of your local setup you will need to setup links between your installation and this repository.  
The following instructions assumes:
1. You are using XAMPP and the xampp location is at c:\xampp\ 
2. Your htdocs folder located at c:\xampp\htdocs\ 
3. A local instance of wordpress for the "myclient" work at c:\xampp\htdocs\myclient
4. Your repository folder located at c:\mygitfolders\myclient

- Create symlink between your git repo theme and location in wp
    - ` mklink /J c:\xampp\htdocs\myclient\wp-content\themes\renameme c:\mygitfolders\myclient\themes\renameme`
- Create a symlink between your git repo plugins and location in wp
    - Delete the plugins folder from your local wp
    -  ` mklink /J c:\xampp\htdocs\myclient\wp-content\plugins c:\mygitfolders\myclient\plugins`
- You are now ready to develop locally.
> Be sure to regularly commit your changes and push to bitbucket

## Step : Add automated deployment to design.brafton.com [ the following are detailed in the deployments repository as well ]
> Note: this step should only be completed when you are ready to move to development on design.brafton.com  
  
> Push your most up to date changes to bitbucket before creating your deployment.

- if you have a copy of the 'deployments' repo pull the recent changes
    - cd into deployments folder `git pull origin master`
- If you do not already have a copy of the 'deployments' repo clone the repo
    - git clone https://{yourusername}@bitbucket.org/braftonclientsites/deployments.git
- copy the `braftonclientsites-repo.deployment.json` file and rename `braftonclientsites-{repoName}.deployment.json`
- Modify the deployment.json file for your deployment
```json
{
    "name": "Must match the name in your hermes.json file",
    "account": "braftonclientsites",
    "repo": "must match the repo name in your hermes.json file",
    "deploy": [
        {
            "tag": "must match the tag in your hermes.json file",
            "branch": "Must match the branch in your hermes.json file",
            "beforeinstall": "",
            "afterinstall": "",
            "source": "",
            "target": ""
        }
    ]
}
```
- Modify the deployment repo's hermes.json configuration file to reflect the correct "Global" variables that will match the configuration you just set up
```json
"global": {
        "newdeployment_repo": "{Repo Name from deployment file}",
        "newdeployment_tag": "{Tag from deployment file}"
    },
```
- run ` git add *`
- run ` git commit -m "adding {name of your deployment}"`
- run git push origin master
- You can now find your site at http://design.brafton.com/myclient [ This can take up to 5 minutes to deploy ]
- Everytime you make a push/merge to the desired branch your changes will be automatically deployed to the design server.

## Step : Package your site up for deploymen to the clients server.
- in the scripts/after.sh file uncomment out the following lines
```bash
cd ${GLOBAL_TARGETROOT} && wp db dump ${NAME}.sql
tar -czf ${GLOBAL_TARGETROOT}/${NAME}.tar.gz ${GLOBAL_TARGETROOT}
```
- the next time you push to bitbucket the above will create a sql dump file in the root directory of the wp site and create a zip file downloadable at http://design.brafton.com/myclient/myclient.tar.gz
- take the zip file and copy the wp-content folder into a live server running wordpress
- take the sql file and import into the clients live server running MySQL
