#!/usr/bin/env bash
NEWSITE=0
if [ "${NAME}" = "" ] || [ "${REPO}" = "" ] || [ "${GLOBAL_TITLE}" = "" ] || [ "${GLOBAL_THEME}" = "" ]
then
    echo "One of the required variables has not been set in hermes.json" 
    exit;
fi 
#create wp if not already there
if [ ! -d "${GLOBAL_TARGETROOT}" ]
then
    cd $HERMES_ROOT && git submodule update --init --recursive
    wp core download --path=${GLOBAL_TARGETROOT} --allow-root
    cd ${GLOBAL_TARGETROOT} && wp core config --dbprefix=${NAME}_ --allow-root
    cd ${GLOBAL_TARGETROOT} && wp core install --url=${GLOBAL_URL} --title="${GLOBAL_TITLE}" --allow-root
    chown -R ${GLOBAL_OWNER}:${GLOBAL_GROUP} ${GLOBAL_TARGETROOT}
    chmod -R 755 ${GLOBAL_TARGETROOT}
    NEWSITE=1
fi
#To deploy plugins uncomment the below lines
#
#cp -R $HERMES_ROOT/plugins/* ${GLOBAL_TARGETROOT}/wp-content/plugins
if [ "${GLOBAL_DEPLOY_NEW_SUBMODULE}" = "y" ]
then
    cd $HERMES_ROOT && git submodule update --init --recursive
fi
#Deploy plugins if deploy is set to "y"
if [ "${GLOBAL_DEPLOY_PLUGINS}" = "y" ] || [[ $NEWSITE = 1 ]]
then
    cp -R $HERMES_ROOT/plugins/* ${GLOBAL_TARGETROOT}/wp-content/plugins
fi

#If new installation or update set to "y" update plugins
if [[ $NEWSITE = 1 ]] || [ "${GLOBAL_UPDATE_PLUGINS}" = "y" ]
then
    cd ${GLOBAL_TARGETROOT} && wp plugin update --all --allow-root
fi
if [[ $NEWSITE = 1 ]]
then
    #Check if there is a parent theme to deploy
    if [ "${GLOBAL_PARENT}" != "" ]
    then
        cp -R $HERMES_ROOT/themes/${GLOBAL_PARENT} ${GLOBAL_TARGETROOT}/wp-content/themes/${GLOBAL_PARENT}
    fi
    #Activate the required plugins
    cd ${GLOBAL_TARGETROOT} && wp plugin activate BraftonWordpressPlugin-master --allow-root
    cd ${GLOBAL_TARGETROOT} && wp plugin activate wp-smushit --allow-root
    cd ${GLOBAL_TARGETROOT} && wp plugin activate wordpress-seo --allow-root
fi

if [ "${GLOBAL_DEPLOY_PARENT}" = "y" ] && [ "${GLOBAL_PARENT}" != "" ]
then
    cp -R $HERMES_ROOT/themes/${GLOBAL_PARENT} ${GLOBAL_TARGETROOT}/wp-content/themes/${GLOBAL_PARENT}
fi
#If your theme requires a parent theme comment out the following
#
#cp $HERMES_ROOT/themes/{themeName} ${GLOBAL_TARGETROOT}/wp-content/{themeName}
