<?php
if(function_exists("register_field_group"))
{
	register_field_group(array (
		'id' => 'acf_page-default',
		'title' => 'Page Default',
		'fields' => array (
			array (
				'key' => 'field_59a32b723c034',
				'label' => 'Main Content',
				'name' => 'main_content',
				'type' => 'wysiwyg',
				'default_value' => '',
				'toolbar' => 'full',
				'media_upload' => 'yes',
			),
			array (
				'key' => 'field_59a1e405314f8page',
				'label' => 'Main Content Image',
				'name' => 'main_image',
				'type' => 'image',
				'save_format' => 'url',
				'preview_size' => 'thumbnail',
				'library' => 'all',
			),
			array (
				'key' => 'field_59e77889ce661',
				'label' => 'Shrink to natural size?',
				'name' => 'shrink',
				'type' => 'checkbox',
				'choices' => array (
					'Yes' => 'Yes',
				),
				'default_value' => '',
				'layout' => 'vertical',
			),
			array (
				'key' => 'field_59a32b723c034sec',
				'label' => 'Secondary Content',
				'name' => 'secondary_content',
				'type' => 'wysiwyg',
				'default_value' => '',
				'toolbar' => 'full',
				'media_upload' => 'yes',
			),
			array (
				'key' => 'field_59a1e405314f8page2',
				'label' => 'Secondary Image',
				'name' => 'secondary_image',
				'type' => 'image',
				'save_format' => 'url',
				'preview_size' => 'thumbnail',
				'library' => 'all',
			),
			array (
				'key' => 'field_59e77889ce662',
				'label' => 'Shrink to natural size?',
				'name' => 'shrink2',
				'type' => 'checkbox',
				'choices' => array (
					'Yes' => 'Yes',
				),
				'default_value' => '',
				'layout' => 'vertical',
			),
			array (
				'key' => 'field_59a32b723c034tert',
				'label' => 'Tertiary Content',
				'name' => 'tertiary_content',
				'type' => 'wysiwyg',
				'default_value' => '',
				'toolbar' => 'full',
				'media_upload' => 'yes',
			),
			array (
				'key' => 'field_59a1e405314f8tert',
				'label' => 'Tertiary Image',
				'name' => 'tertiary_image',
				'type' => 'image',
				'save_format' => 'url',
				'preview_size' => 'thumbnail',
				'library' => 'all',
			),
			array (
				'key' => 'field_59e77889ce663',
				'label' => 'Shrink to natural size?',
				'name' => 'shrink3',
				'type' => 'checkbox',
				'choices' => array (
					'Yes' => 'Yes',
				),
				'default_value' => '',
				'layout' => 'vertical',
			),
			array (
				'key' => 'field_59a32b723c034four',
				'label' => 'Fourth Content',
				'name' => 'fourth_content',
				'type' => 'wysiwyg',
				'default_value' => '',
				'toolbar' => 'full',
				'media_upload' => 'yes',
			),
			array (
				'key' => 'field_59a1e405314f8four',
				'label' => 'Fourth Image',
				'name' => 'fourth_image',
				'type' => 'image',
				'save_format' => 'url',
				'preview_size' => 'thumbnail',
				'library' => 'all',
			),
			array (
				'key' => 'field_59e77889ce663four',
				'label' => 'Shrink to natural size?',
				'name' => 'shrink4',
				'type' => 'checkbox',
				'choices' => array (
					'Yes' => 'Yes',
				),
				'default_value' => '',
				'layout' => 'vertical',
			),
			array (
				'key' => 'field_59a32b723c034tertcta',
				'label' => 'CTA',
				'name' => 'cta_content',
				'type' => 'wysiwyg',
				'default_value' => '',
				'toolbar' => 'full',
				'media_upload' => 'yes',
			),
		),
		'location' => array (
			array (
				array (
					'param' => 'page_template',
					'operator' => '==',
					'value' => 'templates/culture.php',
					'order_no' => 0,
					'group_no' => 0,
				),
			),
			array (
				array (
					'param' => 'page_template',
					'operator' => '==',
					'value' => 'default',
					'order_no' => 0,
					'group_no' => 0,
				),
			),
		),
		'options' => array (
			'position' => 'normal',
			'layout' => 'no_box',
			'hide_on_screen' => array (
			),
		),
		'menu_order' => 0,
	));
}
