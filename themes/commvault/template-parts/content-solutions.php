<?php
/**
 *
 * @link https://codex.wordpress.org/Template_Hierarchy
 *
 * @package Bulmapress
 */
?>

<div class="intro">
    <div class="container has-text-centered">
        <h1><?php the_title(); ?></h1>
        <?php the_content(); ?>
        <div class="solution-buttons">
            <ul>
                <?php echo wp_list_pages( 'sort_column=menu_order&title_li=&child_of=69'); ?>
            </ul>
        </div>
    </div>
</div>  