<?php
/**
 * The header for our theme
 *
 * This is the template that displays all of the <head> section and everything up until <div id="content">
 *
 * @link https://developer.wordpress.org/themes/basics/template-files/#template-partials
 *
 * @package Bulmapress
 */
?>

<!DOCTYPE html>
<html <?php language_attributes(); ?> class="is-fullheight">
<head>
	<meta charset="<?php bloginfo( 'charset' ); ?>">
	<meta name="viewport" content="width=device-width, initial-scale=1">
	<link rel="profile" href="http://gmpg.org/xfn/11">
	<?php wp_head(); ?>
	<script>
	  (function(i,s,o,g,r,a,m){i['GoogleAnalyticsObject']=r;i[r]=i[r]||function(){
	  (i[r].q=i[r].q||[]).push(arguments)},i[r].l=1*new Date();a=s.createElement(o),
	  m=s.getElementsByTagName(o)[0];a.async=1;a.src=g;m.parentNode.insertBefore(a,m)
	  })(window,document,'script','https://www.google-analytics.com/analytics.js','ga');

	  ga('create', 'UA-100347350-2', 'auto');
	  ga('send', 'pageview');
	</script>
	<meta name="google-site-verification" content="quuA41JrR4xcB3KJ69YCt6dVHJ-VOHKUjtO74mKmGfM" />
</head>

<body <?php body_class(); ?>>
	<div id="page" class="site">
		<?php bulmapress_skip_link_screen_reader_text(); ?>
		<header id="header" class="hero">
			<div class="hero-head">
				<div class="container">
					<!--<div class="login column is-full">
						<a href="#" class="button">Customer Login</a>
					</div>-->
					<nav id="site-navigation" class="nav main-navigation" role="navigation">
						<div class="nav-left">
							<?php bulmapress_home_link('nav-item is-brand'); ?>
							<?php // bulmapress_blog_description('nav-item is-muted'); ?>
						</div>
						<div class="phone-header"><i class="fa fa-phone"></i> (866) 431-7646</div>
						<?php bulmapress_menu_toggle(); ?>
						<?php bulmapress_navigation(); ?>
						<a class="search"><i class="fa fa-search"></i></a>
						<div class="searchForm">
							<?php echo get_search_form(); ?>
						</div>
					</nav><!-- #site-navigation -->
				</div><!-- .container -->
			</div><!-- .hero-head -->
		</header><!-- .hero -->

		<div id="spacer"></div>

		<div id="content" class="site-content">
